// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"

#include <cstdio>
#include <exception>
#include <tools/state.h>
#include <filesystem/path.h>
#include "plsdk/sdk.h"

HMODULE ghmSelf = NULL;

BOOL APIENTRY DllMain( HMODULE hModule, DWORD dwReason, LPVOID lpReserved )
{
	ghmSelf = hModule;

	static const char* const reason[] =
	{
		"DLL_PROCESS_DETACH",
		"DLL_PROCESS_ATTACH",
		"DLL_THREAD_ATTACH",
		"DLL_THREAD_DETACH",
	};
	printf( "dummy.dll!DllMain() %s!\n", reason[dwReason] );
	if ( dwReason==DLL_PROCESS_ATTACH ) ::MessageBoxA( NULL, reason[dwReason], "Hello!", MB_OK );

	//PVOID addr = NULL;
	//SIZE_T size = 4096;
	//int nts = plsdk::SystemCall( 0, -1, &addr, 0, &size, MEM_COMMIT, PAGE_EXECUTE_READWRITE );
	//nts = plsdk::SystemCall( 1, -1, &addr, &size, MEM_RELEASE );

	// Testing API Redirection...
	if ( dwReason==DLL_PROCESS_ATTACH )
	{
		HMODULE hm1 = ::GetModuleHandleA( "KERNEL32.DLL" );
		void* pfn = ::GetProcAddress( hm1, "GetModuleHandleW" );
		HMODULE hm2 = ((HMODULE (__stdcall*)( const wchar_t* ))pfn)( L"kernel32.dll" );
		printf( "hm1( 0x%08X ) == hm2( 0x%08X )\n", hm1, hm2 );

		// Try exceptions
		try
		{
			throw std::exception( "I'm an exception!" );
		}
		catch ( const std::exception& err )
		{
			printf( "Exception! %s\n", err.what() );
		}
	}
	
	return TRUE;
}

extern "C" __declspec(dllexport) long __stdcall ExportedFunc( unsigned char* str )
{
	return printf( "%s", str );
}

extern "C" __declspec(dllexport) long __stdcall InjectHookEx( int code, WPARAM w, LPARAM l )
{
	RUNONCE( once )
	{
		HMODULE hModule;
		::GetModuleHandleEx( GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, (LPCSTR)&InjectHookEx, &hModule );
		printf( "Injected at 0x%08X with SetWindowsHookEx!\n", hModule );
	}
	printf( "InjectHookEx() code=%d WPARAM=%d LPARAM=%d\n", code, w, l );
	return ::CallNextHookEx( NULL, code, w, l );
}
