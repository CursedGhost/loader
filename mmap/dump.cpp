#include "stdafx.h"

#include <tools/va_buf.h>
#include "loader.h"
#include "module.h"

class CDumper : public CBaseLoader
{
public:
	typedef CBaseLoader BaseClass;
	CDumper() : dumpRaw(false), dumpModule(false), details(false)
	{
	}
	virtual void PrintDescription( )
	{
		printf( "Dump and reconstruct a DLL from memory.\n"
			"Usage: mmap.exe detail [flags] <process.exe> <address>\n"
			" --raw (-r) Do not reconstruct the DLL, just dump raw memory\n"
			" --module (-m) Reconstruct this memory region as a DLL\n"
			" --details (-d) Print detailed information about the memory regions\n"
			);
		BaseClass::PrintDescription();
	}
	virtual bool Argument( arg_t& arg )
	{
		const char* s = arg.v[arg.i];
		if ( !_stricmp( s, "-r" ) || !_stricmp( s, "--raw" ) )
		{
			dumpRaw = true;
			return ++arg.i <= arg.c;
		}
		else if ( !_stricmp( s, "-m" ) || !_stricmp( s, "--module" ) )
		{
			dumpModule = true;
			return ++arg.i <= arg.c;
		}
		else if ( !_stricmp( s, "-d" ) || !_stricmp( s, "--details" ) )
		{
			details = true;
			return ++arg.i <= arg.c;
		}
		else
		{
			return BaseClass::Argument( arg );
		}
	}
	unsigned long ReadInt( const char* str )
	{
		int radix = 10;
		if ( str[0]=='0' && str[1]=='x' )
			radix = 16;
		char* endptr;
		return strtol( str, &endptr, radix );
	}
	virtual void Action( arg_t& arg )
	{
		// Get initial memory information, discard if invalid
		PVOID base = (PVOID)ReadInt( arg.v[arg.i++] );
		MEMORY_BASIC_INFORMATION info, shared;
		if ( proc.Query( base, info ) && info.State!=MEM_FREE )
		{
			// Reset shared to zero so we can accumulate the regions of an allocation
			memset( &shared, 0, sizeof(shared) );
			int count = 0;
			// Initialize the allocation base
			for ( PVOID addr = shared.AllocationBase = info.AllocationBase;
				// Query this region, stop until we hit a different allocation
				proc.Query( addr, info ) && shared.AllocationBase==info.AllocationBase;
				// Next region
				addr = (PVOID)( (uintptr_t)info.BaseAddress + info.RegionSize ) )
			{
				// Display detailed information
				if ( details )
				{
					if ( info.AllocationBase==info.BaseAddress )
						printf( "Detailed region information\n" );
					printf( "  0x%08X:%06X Protect:%03X State:%s\n",
						info.BaseAddress, info.RegionSize, info.Protect,
						info.State==MEM_COMMIT?"COMMIT":info.State==MEM_RESERVE?"RESERVED":"?" );
				}
				// Accumulate result in shared
				shared.AllocationProtect |= info.AllocationProtect;
				shared.BaseAddress = NULL;
				shared.Protect |= info.Protect;
				shared.RegionSize += info.RegionSize;
				shared.State |= info.State;
				shared.Type |= info.Type;
				count++;
			}

			printf( "Memory Allocation for 0x%08X\n"
				"  RegionCount       = %d\n"
				"  AllocationBase    = 0x%08X\n"
				"  AllocationProtect = 0x%03X\n"
				"  AllocationSize    = 0x%06X\n"
				"  Protect           = 0x%03X\n"
				"  State             = %s\n"
				"  Type              = %s\n",
				base, count,
				shared.AllocationBase, shared.AllocationProtect, shared.RegionSize,
				shared.Protect,
				shared.State==MEM_COMMIT?"COMMIT":shared.State==MEM_RESERVE?"RESERVED":"MIXED",
				shared.Type==MEM_PRIVATE?"Private":shared.Type==MEM_MAPPED?"Mapped":shared.Type==MEM_IMAGE?"Image":"?"
				);

			char szFileName[512];
			if ( GetMappedFileNameA( proc.getHandle(), shared.AllocationBase, szFileName, sizeof(szFileName) ) )
			{
				printf( "  MappedFileName    = %s\n", szFileName );
			}

			DumpData( shared );
		}
		else
		{
			printf( " No allocated memory found at 0x%08X!\n", base );
		}
	}
	void DumpData( const MEMORY_BASIC_INFORMATION& shared )
	{
		if ( dumpRaw )
		{
			DumpRawData( shared );
		}
		else if ( dumpModule )
		{
			// TODO! More output logging!
			CModule md( proc.getHandle(), shared );
			md.DumpPE();
		}
	}
	void DumpRawData( const MEMORY_BASIC_INFORMATION& shared )
	{
		// Allocate enough memory on our end
		PVOID copy = ::VirtualAlloc( NULL, shared.RegionSize, MEM_COMMIT, PAGE_READWRITE );
		// Copy the data
		if ( proc.Read( shared.AllocationBase, copy, shared.RegionSize ) )
		{
			// Autogenerate filename
			auto name = tools::va_printf<64>( "m%08Xs%06X.bin", shared.AllocationBase, shared.RegionSize );
			if ( FILE* h = ::fopen( name, "wb" ) )
			{
				// Write the data to file
				::fwrite( copy, 1, shared.RegionSize, h );
				::fclose( h );
			}
			// Print a nice message
			printf( "Wrote 0x%06X bytes to %s.\n", shared.RegionSize, &name );
		}
		else
		{
			printf( "Failed to read memory, %d bytes read!\n", shared.RegionSize );
		}
		// Cleanup
		::VirtualFree( copy, 0, MEM_FREE );
	}

private:
	bool dumpRaw;
	bool dumpModule;
	bool details;
};

void mainDump( int argc, char* argv[] )
{
	CDumper ldr;
	ldr.Main( argc, argv );
}
