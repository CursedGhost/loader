#include "stdafx.h"

#include "idhook.h"
#include <pelite/lib.h>

CIDHook::CIDHook() : pBaseAddress(nullptr), pRemoteBuffer(NULL), numImportDescs(0)
{
}
CIDHook::~CIDHook()
{
}
bool CIDHook::Setup( HMODULE hModule )
{
	pBaseAddress = (unsigned char*) hModule;

	// Find Import DataDirectory
	IMAGE_DOS_HEADER dos;
	IMAGE_NT_HEADERS nt;
	if ( Read( pBaseAddress, dos ) && Read( pBaseAddress+dos.e_lfanew, nt ) )
	{
		sizeOfImage = nt.OptionalHeader.SizeOfImage;
		// Make a copy of the old import datadirectory
		oImpDir = nt.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT];
		// For convenience save the address of the import datadirectory
		pImpDir = pBaseAddress + dos.e_lfanew+offsetof(IMAGE_NT_HEADERS,OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
		return true;
	}

	return false;
}
bool CIDHook::InitImports()
{
	// Allocate memory for the new import descriptors
	pRemoteBuffer = (unsigned char*) Allocate();
	if ( pRemoteBuffer )
	{
		// Copy old Import Descriptors
		IMAGE_IMPORT_DESCRIPTOR desc;
		numImportDescs = 0;
		do
		{
			Read( pBaseAddress + oImpDir.VirtualAddress + numImportDescs*sizeof(desc), desc );
			Write( pRemoteBuffer + numImportDescs*sizeof(desc), desc );
			++numImportDescs;
		}
		while ( desc.FirstThunk!=0 && desc.OriginalFirstThunk!=0 && numImportDescs*sizeof(IMAGE_IMPORT_DESCRIPTOR)<oImpDir.Size );

		// Remove terminating entry from our count
		--numImportDescs;

		// Initialize new Import DataDirectory
		newImpDir.Size = oImpDir.Size;
		newImpDir.VirtualAddress = pRemoteBuffer - pBaseAddress;

		// Reserve some memory where we can put IAT and names of the imports
		// FIXME! Assumes no more than 31 modules can be imported total!
		pRemoteAlloc = pRemoteBuffer + 32*sizeof(desc);

		return true;
	}
	return false;
}
bool CIDHook::ImportDLL( filesystem::path::handle path )
{
	// Check if we reached the limit...
	if ( numImportDescs>=31 )
	{
		return false;
	}

	// Get any export from the dll we want to inject.
	// We must import at least 1 symbol or windows will not load our module!
	symbol_t sym;
	if ( !GetAnyExport( path, sym ) )
	{
		return false;
	}

	// Fill out the descriptor
	IMAGE_IMPORT_DESCRIPTOR desc;
	desc.TimeDateStamp = 0;
	desc.ForwarderChain = 0;
	// Allocate the name of the dll to import from
	unsigned char* pathbuffer = Malloc( sizeof(path), desc.Name );
	// Allocate the IAT
	desc.OriginalFirstThunk = 0; // The INT (OriginalFirstThunk) can be ignored!
	IMAGE_THUNK_DATA import[2] = {0,0};
	IMAGE_THUNK_DATA* iat = (IMAGE_THUNK_DATA*) Malloc( sizeof(import), desc.FirstThunk );
	// Allocate the name to be imported
	IMAGE_IMPORT_BY_NAME* ent = (IMAGE_IMPORT_BY_NAME*) Malloc( sizeof(sym), import[0].u1.AddressOfData );
	// Append a new import descriptor
	IMAGE_IMPORT_DESCRIPTOR* entry = (IMAGE_IMPORT_DESCRIPTOR*)pRemoteBuffer + (numImportDescs++);
	// Commit the new import descriptor
	Write<IMAGE_IMPORT_DESCRIPTOR>( entry, desc );
	Write<filesystem::path>( pathbuffer, path );
	Write<IMAGE_THUNK_DATA[2]>( iat, import );
	Write<symbol_t>( ent, sym );
	// Update directory size
	newImpDir.Size += sizeof(desc);
	return true;
}
bool CIDHook::Commit()
{
	return Write( pImpDir, newImpDir );
}
bool CIDHook::Restore()
{
	return Write( pImpDir, oImpDir );
}
bool CIDHook::GetAnyExport( filesystem::path::handle path, symbol_t& sym )
{
	if ( FILE* h = ::fopen( path.c_str(), "rb" ) ) {
	using namespace pelite;
	PeFileRaw pe;
	pe.Init( h );
	::fclose( h );
	ImageDataDirectory& edir = pe.OptionalHeader()->DataDirectory[DataDir_Export];
	if ( edir.VirtualAddress ) {
	ImageExportDirectory* exp = pe.RvaToPtr<ImageExportDirectory*>( edir.VirtualAddress );
	if ( exp->NumberOfNames>0 && exp->AddressOfNames && exp->AddressOfNameOrdinals ) {

	unsigned int* names = pe.RvaToPtr<unsigned int*>( exp->AddressOfNames );
	WORD* nameord = pe.RvaToPtr<WORD*>( exp->AddressOfNameOrdinals );

	char* any = pe.RvaToPtr<char*>( names[0] );
	strncpy( sym.name, any, sizeof(sym.name) );
	sym.ordinal = nameord[0] + exp->Base;

	return true;
	}
	}
	}
	return false;
}
unsigned char* CIDHook::Malloc( size_t bytes, DWORD& rva )
{
	// Round to dword bounds
	bytes = (bytes&~3)+4;
	// Reserve
	unsigned char* p = pRemoteAlloc;
	pRemoteAlloc += bytes;
	// hModule+rva=p
	rva = p - pBaseAddress;
	return p;
}

