#pragma once

#include <Windows.h>
#include "filesystem/path.h"

// Import Descriptor Hijack
class CIDHook
{
public:
	CIDHook();
	virtual ~CIDHook();

	virtual bool Read( const void* pAddress, void* pBuffer, size_t bytes ) = 0;
	virtual bool Write( void* pAddress, const void* pBuffer, size_t bytes ) = 0;
	virtual void* Allocate() = 0;

	template< typename T >
	inline bool Read( const void* pAddress, T& t ) {
		return Read( pAddress, &t, sizeof(t) );
	}

	template< typename T >
	inline bool Write( void* pAddress, const T& t ) {
		return Write( pAddress, &t, sizeof(t) );
	}

	struct symbol_t
	{
		WORD ordinal;
		char name[128];
	};

	bool Setup( HMODULE hModule );
	bool InitImports();
	// Must be a full path!
	bool ImportDLL( filesystem::path::handle path );
	bool Commit();
	bool Restore();
	bool GetAnyExport( filesystem::path::handle path, symbol_t& sym );
	unsigned char* Malloc( size_t bytes, DWORD& rva );

protected:
	unsigned char* pBaseAddress;
	unsigned int sizeOfImage;

	PVOID pImpDir;
	IMAGE_DATA_DIRECTORY oImpDir, newImpDir;

	unsigned char* pRemoteBuffer;
	unsigned char* pRemoteAlloc;
	size_t numImportDescs;
};
