#include "stdafx.h"

#include "loader.h"



//----------------------------------------------------------------
// CreateRemoteThread / LoadLibrary injector
//----------------------------------------------------------------

class CLoaderInject : public CBaseLoader
{
public:
	typedef CBaseLoader BaseClass;
	CLoaderInject()
	{
	}
	virtual void PrintDescription()
	{
		printf( "Plain dll injector using CreateRemoteThread/LoadLibrary method.\n"
			"Usage: mmap.exe inject [flags] <process.exe> [bin1.dll] [bin2.dll] ...\n" );
		BaseClass::PrintDescription();
	}
	virtual void Action( arg_t& arg )
	{
		for ( ; arg.i<arg.c; ++arg.i )
		{
			filesystem::hpath file = arg.v[arg.i];

			// Attempt injection
			plsdk::Manager mgr( proc, false );
			if ( void* hmod = mgr.InjectDll( file ) )
			{
				printf( " Injected '%s', hModule = 0x%08X\n", file, hmod );
			}
			else
			{
				printf( " Failed to inject '%s' with error %X!\n", file, ::GetLastError() );
			}
		}
	}
};





//----------------------------------------------------------------
// SetWindowsHookEx injector
//----------------------------------------------------------------

class CLoaderWinHooks : public CBaseLoader
{
public:
	typedef CBaseLoader BaseClass;
	CLoaderWinHooks() : functionName(nullptr)
	{
	}
	virtual void PrintDescription()
	{
		printf( "Using SetWindowsHookEx dll injector.\n"
			"Usage: mmap.exe winhooks [flags] <process.exe> [bin1.dll] [bin2.dll] ...\n"
			" --callback (-fn) binaries to inject must export this function for use with winhooks.\n" );
		BaseClass::PrintDescription();
	}
	virtual bool Argument( arg_t& arg )
	{
		const char* s = arg.v[arg.i];
		if ( streq( s, "--callback" ) || streq( s, "-fn" ) )
		{
			functionName = arg.v[++arg.i];
			return ++arg.i<arg.c;
		}
		else
		{
			return BaseClass::Argument( arg );
		}
	}
	virtual bool Attach( unsigned long pid, unsigned long tid, arg_t& arg )
	{
		// Check requirements
		if ( !functionName )
		{
			printf( " No --callback present, failed to inject!\n" );
			return false;
		}
		if ( !tid )
		{
			printf( " No threadid available, please use --window!\n" );
			return false;
		}
		Action( pid, tid, arg );
		return true;
	}
	virtual void Action( arg_t& arg )
	{
		assert( false );
	}
	void Action( unsigned long pid, unsigned long tid, arg_t& arg )
	{
		for ( ; arg.i<arg.c; ++arg.i )
		{
			filesystem::hpath file = arg.v[arg.i];

			// Attempt injection
			if ( void* hMod = InjectDllEx( file, functionName, pid, tid ) )
			{
				printf( " Injected '%s', hModule = 0x%08X\n", file, hMod );
			}
			else
			{
				printf( " Failed to inject '%s' with error %X!\n"
					" Please make sure that you read the documentation carefully for its requirements!\n",
					file, ::GetLastError() );
			}
		}
	}
	void* InjectDllEx( filesystem::hpath path, const char* hookfn, unsigned long pid, unsigned long tid )
	{
		void* hMod = nullptr;
		// First load the DLL in our own process
		if ( HMODULE hmDll = ::LoadLibraryA( path ) )
		{
			// Get the function we register our hook for
			if ( HOOKPROC pfnHook = (HOOKPROC) ::GetProcAddress( hmDll, hookfn ) )
			{
				// Register a windows hook
				if ( HHOOK hk = ::SetWindowsHookExA( WH_CALLWNDPROC, pfnHook, hmDll, tid ) )
				{
					// Wait (10ms*500 = 5s) for windows to inject the dll...
					// FIXME! Find a way to force this trigger?!
					for ( int i = 0; i<500; ++i )
					{
						// Wait for the DLL to get loaded
						HANDLE snap = INVALID_HANDLE_VALUE;
						MODULEENTRY32 me;
						bool found = plsdk::Process::EnumModules( path, pid, snap, me );
						::CloseHandle( snap );
						if ( found )
						{
							hMod = me.hModule;
							break;
						}
						Sleep( 10 );
					}
					// You're supposed to LoadLibrary on yourself to increment your reference count or you'll be unloaded!
					::UnhookWindowsHookEx( hk );
				}
			}
			::FreeLibrary( hmDll );
		}
		return hMod;
	}
	const char* functionName;
};





//----------------------------------------------------------------
// CreateRemoteThread/FreeLibrary ejector
//----------------------------------------------------------------

class CLoaderEject : public CBaseLoader
{
public:
	typedef CBaseLoader BaseClass;
	CLoaderEject() : force(false)
	{
	}
	virtual void PrintDescription()
	{
		printf( "Plain dll ejector using CreateRemoteThread/FreeLibrary method.\n"
			"Usage: mmap.exe eject [flags] <process.exe> [bin1.dll] [bin2.dll] ...\n"
			" --force (-f) will ensure the dll is ejected by calling FreeLibrary as many times as is needed.\n" );
		BaseClass::PrintDescription();
	}
	virtual bool Argument( arg_t& arg )
	{
		const char* s = arg.v[arg.i];
		if ( streq( s, "--force" ) || streq( s, "-f" ) )
		{
			force = true;
			return ++arg.i <= arg.c;
		}
		return BaseClass::Argument( arg );
	}
	virtual void Action( arg_t& arg )
	{
		for ( ; arg.i<arg.c; ++arg.i )
		{
			filesystem::hpath file = arg.v[arg.i];

			// Attempt ejection
			if ( EjectDll( file, force ) )
			{
				printf( " Ejected '%s' success!\n", file );
			}
			else
			{
				printf( " Failed to eject '%s'!\n", file );
			}
		}
	}
	bool EjectDll( filesystem::hpath file, bool force )
	{
		// WARNING! Do not call this shit on arbitrary dlls as it will fuck things up!
		// FIXME! The looping is all wrong, I just need to loop 'me.ProccntUsage' times to eject the dll...

		// Some system dlls cannot be ejected so we cannot loop forever trying to FreeLibrary
		// Limit the max number of successful retries to 16
		int tries = 16;
		int found;
		do
		{
			// Find module address
			MODULEENTRY32 me;
			HANDLE snap = INVALID_HANDLE_VALUE;
			found = 0;
			while ( proc.EnumModules( file, proc.getProcessID(), snap, me ) )
			{
				// Call FreeLibrary with the module handle
				// Address should be the same even with ASLR...
				if ( HANDLE hThread = proc.CreateThread( (LPTHREAD_START_ROUTINE)&FreeLibrary, me.hModule ) )
				{
					// Wait for it to finish
					// FIXME! Do we really need to wait?
					::WaitForSingleObject( hThread, INFINITE );
					// No exit code as FreeLibrary has a void return type
					// Cleanup thread handle
					::CloseHandle( hThread );
					// Need to know how many we found
					++found;
				}
			}
		}
		while ( force && ( found && --tries ) );
		return found>0 && tries>0;
	}
protected:
	bool force;
};

void mainInject( int argc, char* argv[] )
{
	CLoaderInject ldr;
	ldr.Main( argc, argv );
}
void mainWinHooks( int argc, char* argv[] )
{
	CLoaderWinHooks ldr;
	ldr.Main( argc, argv );
}
void mainEject( int argc, char* argv[] )
{
	CLoaderEject ldr;
	ldr.Main( argc, argv );
}

