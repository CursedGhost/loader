#include "stdafx.h"

#include "loader.h"
#include "idhook.h"

class CIDHookLaunch : public CIDHook
{
public:
	CIDHookLaunch( plsdk::Process& proc ) : proc(proc)
	{
	}
	virtual ~CIDHookLaunch()
	{
		if ( pRemoteBuffer )
		{
			proc.Free( pRemoteBuffer, 0, MEM_FREE );
		}
	}
	virtual bool Read( const void* pAddress, void* pBuffer, size_t bytes )
	{
		return proc.Read( pAddress, pBuffer, bytes );
	}
	virtual bool Write( void* pAddress, const void* pBuffer, size_t bytes )
	{
		// Since we don't know if address will be writable just protect it anyway...
		DWORD old;
		bool s = false;
		if ( proc.Protect( pAddress, bytes, PAGE_READWRITE, old ) )
		{
			s = proc.Write( pAddress, pBuffer, bytes );
			proc.Protect( pAddress, bytes, old, old );
		}
		return s;
	}
	virtual void* Allocate()
	{
		// FIXME! I seem to be unable to allocate memory right after the module ends!
		//        Even though I've checked and that memory is unclaimed and available!?!
		PVOID ideal = pBaseAddress + sizeOfImage + 0x000F0000;
		PVOID pmem = proc.Alloc( ideal, 0x4000, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE );
		return pmem;
	}

private:
	plsdk::Process& proc;
};

class CLaunchInject : public CBaseLoader
{
public:
	typedef CBaseLoader BaseClass;
	CLaunchInject() : exefilename(nullptr), options(nullptr)
	{
	}
	virtual void Process( arg_t& arg )
	{
		// Interpret process argument as path to executable
		exefilename = arg.v[arg.i++];
		// Current directory is process path without executable filename.
		filesystem::path cd;
		filesystem::path exe;
		if ( filesystem::make_path(exefilename).is_absolute() )
		{
			cd = exefilename;
			exe = exefilename;
			cd.remove_filename();
		}
		else
		{
			::GetCurrentDirectoryA( sizeof(cd), cd.c_str() );
			exe = cd;
			exe.append( exefilename );
		}
		// Create the process
		STARTUPINFO si = { sizeof(si) };
		if ( ::CreateProcessA( exe.c_str(), options, NULL, NULL, FALSE, CREATE_SUSPENDED, NULL, cd.c_str(), &si, &pi ) )
		{
			printf( "---- CREATE '%s' (%d) ----\n",
				filesystem::make_path(exefilename).filename(), pi.dwProcessId );
			// A lot of crap is not yet initialized, this means we can add a new import to the main executable and let windows do the rest
			Attach( pi.dwProcessId, pi.dwThreadId, arg );
			// Resume the process (should we kill it on failure?)
			::ResumeThread( pi.hThread );
			// Cleanup
			::CloseHandle( pi.hProcess );
			::CloseHandle( pi.hThread );
		}
		else
		{
			printf( "Failed to create the new process (error: 0x%X)!\n", ::GetLastError() );
		}
	}
	virtual void PrintDescription()
	{
		printf( "Create a suspended process and inject by IAT patching.\n"
			"Usage: mmap.exe launch [flags] <process.exe> [bin1.dll] [bin2.dll] ...\n"
			" --command-line (-cmd) command line arguments to launch the process with.\n" );
	}
	virtual bool Argument( arg_t& arg )
	{
		char* s = arg.v[arg.i];
		// Launch a new process with these launch params
		if ( !_stricmp( s, "-cmd" ) || !_stricmp( s, "--command-line" ) )
		{
			options = arg.v[++arg.i];
			return ++arg.i < arg.c;
		}
		return false;
	}
	virtual void Action( arg_t& arg )
	{
		// Find the main executable
		HMODULE hModule = FindModule( filesystem::make_path(exefilename).filename() );
		if ( hModule )
		{
			CIDHookLaunch hook( proc );
			if ( !hook.Setup( hModule ) )
			{
				printf( "Error (0x%X) reading the PE header (0x%08X)!\n", ::GetLastError(), hModule );
			}
			else if ( !hook.InitImports() )
			{
				printf( "Error (0x%X) allocating temp memory and reading import descriptors!\n", ::GetLastError() );
			}
			else
			{
				// Add dlls to be loaded
				QueueDLLs( hook, arg );
				// Commit the changes
				hook.Commit();
				// Let the windows loader load everything
				::ResumeThread( pi.hThread );
				// Wait until windows has loaded our DLLs
				int timeout = 1000;
				if ( !WaitDLLs( hook, arg, timeout ) )
				{
					printf( "Error, injected modules not found after waiting %d ms!\n", timeout );
				}
				// Restore our changes
				hook.Restore();
			}
		}
	}
	void QueueDLLs( CIDHook& hook, arg_t arg )
	{
		for ( ; arg.i<arg.c; ++arg.i )
		{
			// Get full DLL path
			auto file = filesystem::make_path(arg.v[arg.i]);
			filesystem::path path;
			if ( file.is_absolute() )
			{
				path = file;
			}
			else
			{
				::GetCurrentDirectoryA( sizeof(path), path.buffer );
				path.append( file );
			}
			// Try to queue it for importing
			if ( hook.ImportDLL( path ) )
			{
				printf( " Queued '%s' to be imported.\n", &file );
			}
			else
			{
				printf( " Error queueing '%s', make sure it has at least 1 export by name.\n", &file );
			}
		}
	}
	bool WaitDLLs( CIDHook& hook, const arg_t& arg, int ms )
	{
		// Simply look through the dlls we wanted to inject and wait until all of them are available.
		bool s = false;
		for ( ; ms>0 && !s; Sleep( 10 ), ms -= 10 )
		{
			s = true;
			for ( arg_t a = arg; a.i<a.c; ++a.i )
			{
				const char* filename = a.v[a.i];
				if ( !FindModule( filename ) )
				{
					s = false;
					break;
				}
			}
		}
		return ms>0;
	}
	HMODULE FindModule( const char* filename )
	{
		// Cannot access typical CreateToolhelp32Snapshot, so just query memory!

		MEMORY_BASIC_INFORMATION info;
		PVOID base = NULL;

		for ( LPCVOID addr = NULL;
			proc.Query( addr, info );
			addr = (LPCVOID)( ((uintptr_t)info.BaseAddress) + info.RegionSize ) )
		{
			if ( info.Type==MEM_IMAGE )
			{
				char buffer[512];
				if ( ::GetMappedFileNameA( proc.getHandle(), info.AllocationBase, buffer, sizeof(buffer) )>0 )
				{
					const char* file = filesystem::make_path(buffer).filename();
					if ( !_stricmp( file, filename ) )
					{
						return (HMODULE) info.AllocationBase;
					}
				}
			}
		}

		return NULL;
	}
private:
	char* exefilename;
	char* options;
	PROCESS_INFORMATION pi;
};

void mainLaunch( int argc, char* argv[] )
{
	CLaunchInject ldr;
	ldr.Main( argc, argv );
}
