#include "stdafx.h"

#include "loader.h"
#include "plsdk/manager.h"

class CFindWindow
{
public:
	CFindWindow( DWORD pid, const char* match, bool partial )
		: match(match), partial(partial), pid(pid), tid(0) {}

	bool Find()
	{
		if ( match && ::EnumWindows( &EnumWindowsProc, (LPARAM)this ) )
		{
			printf( " No window found for this process!\n" );
			return false;
		}
		return true;
	}

	static BOOL CALLBACK EnumWindowsProc( HWND hWnd, LPARAM lParam )
	{
		CFindWindow& self = *(CFindWindow*)lParam;

		// Get window title
		char title[512];
		if ( ::GetWindowTextA( hWnd, title, sizeof(title) )>0 )
		{
			// Match against input
			if ( CBaseLoader::StringMatch( title, self.match, self.partial ) )
			{
				DWORD pid, tid = ::GetWindowThreadProcessId( hWnd, &pid );
				// Match against process id
				if ( self.pid==pid ) {
					self.tid = tid;
					printf( " Window: '%s' (%d)\n", title, self.tid );
					// Stop at first sign of matching window
					return FALSE;
				}
			}
		}

		return TRUE;
	}
public:
	const char* match;
	bool partial;
	DWORD pid, tid;
};

void CBaseLoader::Main( int argc, char* argv[] )
{
	if ( argc<3 )
	{
		PrintDescription();
		return;
	}
	
	arg_t arg = { argc, argv, 2 };
	if ( !ReadArgs( arg ) )
	{
		printf( "Invalid arguments! Please read the documentation carefully.\n" );
		return;
	}

	// Try to get SeDebugPrivilege if requested
	if ( !GetDebugPrivilege() )
	{
		printf( "Debug privilege was requested, but denied. Run as administrator?\n" );
	}

	Process( arg );
}
void CBaseLoader::Process( arg_t& arg )
{
	unsigned int hits = 0, count = 0;
	const char* match = arg.v[arg.i++];
	printf( "Looking for processes matching '%s'...\n", match );

	// Enumerate over all valid processes
	HANDLE snap = INVALID_HANDLE_VALUE;
	PROCESSENTRY32 pe;
	while ( plsdk::Process::EnumProcess( nullptr, snap, pe ) )
	{
		// For each process
		if ( StringMatch( pe.szExeFile, match, partialMatch ) )
		{
			printf( "---- PROCESS %s (%d) ----\n", pe.szExeFile, pe.th32ProcessID );
			++hits;

			// Optionally provide a window resource
			CFindWindow fndw( pe.th32ProcessID, useWindowName, partialMatch );
			if ( fndw.Find() )
			{
				++count;
				Attach( pe.th32ProcessID, fndw.tid, arg );
			}
		}
	}

	// Print response
	if ( hits ) printf( "--------------------------------\n" );
	if ( !count ) printf( "No matching process found!\n" );
	else printf( "Processed %d process%s!\n", count, (count!=1)?"es":"" );
}
void CBaseLoader::PrintDescription()
{
	printf( " --window-title (-wt) provide a window reference by title.\n"
		    " --partial-match (-pm) case-sensitive substring compare.\n" );
}
bool CBaseLoader::ReadArgs( arg_t& arg )
{
	// This should be in the constructor
	useWindowName = false;
	partialMatch = false;

	// While we have arguments and we haven't hit the last one
	while ( arg.i<arg.c && *(arg.v[arg.i])=='-' )
	{
		// Process each argument
		if ( !Argument( arg ) )
		{
			// Unknown argument
			printf( " Unknown argument %s!\n", arg.v[arg.i] );
			return false;
		}
	}
	return true;
}
bool CBaseLoader::Argument( arg_t& arg )
{
	char* s = arg.v[arg.i];
	if ( streq( s, "-wt" ) || streq( s, "--window-title" ) )
	{
		++arg.i;
		useWindowName = arg.v[arg.i];
		return ++arg.i < arg.c;
	}
	if ( streq( s, "-pm" ) || streq( s, "--partial-match" ) )
	{
		++arg.i;
		partialMatch = true;
		return true;
	}
	return false;
}

bool CBaseLoader::Attach( unsigned long pid, unsigned long tid, arg_t& arg )
{
	// Try to attach
	if ( proc.Attach( pid, tid ) )
	{
		// Perform action
		Action( arg );

		proc.Detach();
		return true;
	}
	else
	{
		printf( " Error accessing process! Error: %X\n", ::GetLastError() );
		return false;
	}
}

//void CBaseLoader::Verify( const arg_t& arg )
//{
//	unsigned int total = 0;
//	unsigned int count = arg.c - arg.i;
//
//	HANDLE hSnap = ::CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, proc.getProcessID() );
//	if ( hSnap!=INVALID_HANDLE_VALUE )
//	{
//		MODULEENTRY32 me;
//		me.dwSize = sizeof(me);
//		for ( unsigned int i = 0; i<count; ++i )
//		{
//			const char* filename = filesystem::make_path(arg.v[arg.i+i]).filename();
//			bool found = false;
//
//			if ( ::Module32First( hSnap, &me ) )
//			{
//				do
//				{
//					if ( streq( filename, me.szModule ) )
//					{
//						found = true;
//						break;
//					}
//				}
//				while ( ::Module32Next( hSnap, &me ) );
//			}
//
//			total += found;
//			if ( talk )
//			{
//				printf( found ? " Injected '%s' at 0x%08X\n" : " Failed to inject '%s'!\n", filename, me.hModule );
//			}
//		}
//		::CloseHandle( hSnap );
//	}
//}


// Credits to MSDN
bool CBaseLoader::SetPrivilege( HANDLE hToken, LPCTSTR Privilege, BOOL bEnablePrivilege )
{
	LUID luid;
	TOKEN_PRIVILEGES tp, tpPrevious;
	DWORD cbPrevious = sizeof(TOKEN_PRIVILEGES);

	if ( !::LookupPrivilegeValue( NULL, Privilege, &luid ) )
		return false;

	// First pass, get current privilege setting
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	tp.Privileges[0].Attributes = 0;

	::AdjustTokenPrivileges( hToken, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), &tpPrevious, &cbPrevious );

	if ( GetLastError()!=ERROR_SUCCESS )
		return false;

	// Second pass, set privilege based on previous setting
	tpPrevious.PrivilegeCount = 1;
	tpPrevious.Privileges[0].Luid = luid;

	if ( bEnablePrivilege )
	{
		tpPrevious.Privileges[0].Attributes |= (SE_PRIVILEGE_ENABLED);
	}
	else
	{
		tpPrevious.Privileges[0].Attributes ^= ( SE_PRIVILEGE_ENABLED & tpPrevious.Privileges[0].Attributes );
	}

	::AdjustTokenPrivileges( hToken, FALSE, &tpPrevious, cbPrevious, NULL, NULL );

	if ( ::GetLastError()!=ERROR_SUCCESS )
		return false;

	return true;
}
bool CBaseLoader::GetDebugPrivilege()
{
	HANDLE hToken, hThread = ::GetCurrentThread();

	if ( !::OpenThreadToken( hThread, TOKEN_ADJUST_PRIVILEGES|TOKEN_QUERY, FALSE, &hToken ) )
	{
		if ( ::GetLastError()==ERROR_NO_TOKEN )
		{
			if ( !::ImpersonateSelf( SecurityImpersonation ) )
			{
				return false;
			}

			if ( !::OpenThreadToken( hThread, TOKEN_ADJUST_PRIVILEGES|TOKEN_QUERY, FALSE, &hToken ) )
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	// Enable SeDebugPrivilege
	bool s = SetPrivilege( hToken, SE_DEBUG_NAME, TRUE );
	::CloseHandle( hToken );
	return s;
}
bool CBaseLoader::streq( const char* s1, const char* s2 )
{
	return _stricmp( s1, s2 )==0;
}
bool CBaseLoader::StringMatch( const char* text, const char* match, bool partial )
{
	return ( !partial && streq( text, match ) ) ||
		   ( partial && strstr( text, match ) );
}
