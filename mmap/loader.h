#pragma once

// Shared loader code

#include "plsdk/manager.h"
#include "filesystem/file.h"

struct arg_t
{
	int c;
	char** v;
	int i;
};

class CBaseLoader
{
public:
	void Main( int argc, char* argv[] );
	virtual void Process( arg_t& arg );

	virtual void PrintDescription( ) = 0;

	// Process all arguments
			bool ReadArgs( arg_t& arg );
	// Process an argument, return true if you handled it
	virtual bool Argument( arg_t& arg );

	virtual bool Attach( unsigned long pid, unsigned long tid, arg_t& arg );
	virtual void Action( arg_t& arg ) = 0;

	// Verify injection the dlls
	//virtual void Verify( const arg_t& arg );

	bool SetPrivilege( HANDLE hToken, LPCTSTR Privilege, BOOL bEnablePrivilege );
	bool GetDebugPrivilege();

	// Helper...
	static bool streq( const char* s1, const char* s2 );

	static bool StringMatch( const char* text, const char* match, bool partial );

protected:
	plsdk::Process proc;
	char* useWindowName;
	bool partialMatch;
};
