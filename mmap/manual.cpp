#include "stdafx.h"

#include "loader.h"
#include "filesystem/filecstd.h"

class CLoaderMap : public CBaseLoader
{
public:
	typedef CBaseLoader BaseClass;
	virtual void PrintDescription( )
	{
		printf( "Manual dll injector using a payload.\n"
			"Usage: mmap.exe map [flags] <process.exe> [bin1.dll] [bin2.dll] ...\n"
			" --payload (-p) specify the path of the payload.dll that comes with this project.\n"
			" --temp (-t) specify the temporary path to dump the payload.dll to, %%x is available for formatting a random number.\n"
			" --flags (-f) specify flags to pass to the manual mapper (no docs yet, sorry).\n" );
		BaseClass::PrintDescription();
	}
	virtual bool Argument( arg_t& arg )
	{
		const char* s = arg.v[arg.i];

		if ( !_stricmp( s, "-p" ) || !_stricmp( s, "--payload" ) )
		{
			plfile = arg.v[++arg.i];

			if ( !pl.open( plfile, filesystem::MODE_READ|filesystem::MODE_BINARY ) )
			{
				printf( "Failed to open '%s'!\n", plfile );
				return false;
			}
			++arg.i;
			return true;
		}
		else if ( !_stricmp( s, "-t" ) || !_stricmp( s, "--temp" ) )
		{
			hint = arg.v[++arg.i];
			return ++arg.i < arg.c;
		}
		else if ( !_stricmp( s, "-f" ) || !_stricmp( s, "--flags" ) )
		{
			flags = atoi( arg.v[++arg.i] );
			return ++arg.i < arg.c;
		}
		else
		{
			return BaseClass::Argument( arg );
		}
	}
	virtual void Action( arg_t& arg )
	{
		plsdk::Manager mgr( proc, false );
		if ( mgr.Q_Begin( &pl, hint ) )
		{
			for ( ; arg.i<arg.c; ++arg.i )
			{
				// Attempt injection
				filesystem::filecstd bin;
				const char* binfile = arg.v[arg.i];
				if ( bin.open( binfile, filesystem::MODE_READ|filesystem::MODE_BINARY ) )
				{
					plsdk::Manager::Error err;
					if ( void* hmod = mgr.Q_MapDll( &bin, flags, err ) )
					{
						printf( " Injected '%s', hModule = 0x%08X\n", binfile, hmod );
					}
					else
					{
						printf( " Failed to inject '%s' with error %08X:\n '%s'!\n", binfile, err.code, err.buf );
					}
				}
				else
				{
					printf( " Failed to open dll '%s'!\n", binfile );
				}
			}
			mgr.Q_End();
		}
		else
		{
			printf( " Failed to launch payload!\n" );
		}
	}
protected:
	const char* plfile;
	const char* hint;
	int flags;
	filesystem::filecstd pl;
};

void mainMap( int argc, char* argv[] )
{
	CLoaderMap ldr;
	ldr.Main( argc, argv );
}
