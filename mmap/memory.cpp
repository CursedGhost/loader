#include "stdafx.h"

#include <tools/va_buf.h>
#include "loader.h"
#include "module.h"

class CScanMemory : public CBaseLoader
{
public:
	typedef CBaseLoader BaseClass;
	CScanMemory() : ignoreSystem(false), showMemory(false)
	{
	}
	virtual void PrintDescription( )
	{
		printf( "Analyze memory from a process with manual map detection.\n"
			"Usage: mmap.exe scan [flags] <process.exe>\n"
			" --system (-s) ignores system dlls\n"
			" --verbose (-v) show all memory allocations\n" );
		BaseClass::PrintDescription();
	}
	virtual bool Argument( arg_t& arg )
	{
		const char* s = arg.v[arg.i];

		if ( !_stricmp( s, "-s" ) || !_stricmp( s, "--system" ) )
		{
			ignoreSystem = true;
			return ++arg.i <= arg.c;
		}
		else if ( !_stricmp( s, "-v" ) || !_stricmp( s, "--verbose" ) )
		{
			showMemory = true;
			return ++arg.i <= arg.c;
		}
		else
		{
			return BaseClass::Argument( arg );
		}
	}
	virtual void Action( arg_t& arg )
	{
		MEMORY_BASIC_INFORMATION info, shared;
		PVOID base = NULL;

		for ( LPCVOID addr = NULL;
			proc.Query( addr, info );
			addr = (LPCVOID)( ((uintptr_t)info.BaseAddress) + info.RegionSize ) )
		{
			// We've scanned a whole allocation block
			if ( base!=info.AllocationBase )
			{
				// Actual allocations, skip free regions
				if ( base && shared.State!=MEM_FREE && shared.Type )
				{
					PrintInformation( shared );
				}
				base = info.AllocationBase;

				shared = info;
			}
			else if ( base )
			{
				// Merge all regions together
				if ( info.AllocationBase!=shared.AllocationBase )
					shared.AllocationBase = NULL;
				if ( info.AllocationProtect!=shared.AllocationProtect )
					shared.AllocationProtect = 0;
				if ( info.BaseAddress!=shared.BaseAddress )
					shared.BaseAddress = NULL;
				shared.Protect |= info.Protect;
				shared.RegionSize += info.RegionSize;
				shared.State |= info.State;
				// Type (Image/Mapped/Private) should remain the same for the whole allocation!
				assert( shared.Type==info.Type ); //shared.Type |= info.Type;
			}
			// Extra check required, sometimes we get to wrap around
			if ( static_cast<intptr_t>(info.RegionSize)<=0 )
			{
				break;
			}
		}
	}
	// Analyze this piece of memory.
	// Return indicates this allocation is a module
	bool Analyze( const MEMORY_BASIC_INFORMATION& shared )
	{
		// This is an image, assume it's a DLL (?)
		if ( shared.Type==MEM_IMAGE )
		{
			return true;
		}
		else if ( shared.Type==MEM_PRIVATE && (shared.State&MEM_COMMIT) )
		{
			unsigned char data[0x1000];
			//for ( uintptr_t addr = (uintptr_t)shared.AllocationBase, end = addr + shared.RegionSize; addr<end; addr += 0x1000 )
			{
				// Will fail on PAGE_GUARD and others...
				if ( proc.Read( shared.AllocationBase, data ) )
				{
					// Check PE header flags
					if ( CModule::Analyze( data, 0 ) )
						return true;
				}
			}
		}
		return false;
	}
	void PrintInformation( const MEMORY_BASIC_INFORMATION& shared )
	{
		// Try to get a filename for MEM_MAPPED and MEM_IMAGE allocations.
		char szFileName[512];
		bool bFileName = ::GetMappedFileNameA( proc.getHandle(), shared.AllocationBase, szFileName, sizeof(szFileName) )!=0;
		bool bIsDLL;

		// Analyze the memory to detect DLLs
		if ( bIsDLL = Analyze( shared ) )
		{
			if ( bFileName )
			{
				if ( ignoreSystem && IsSystemFile( szFileName ) )
				{
					return;
				}
			}
		}
		else if ( showMemory )
		{
		}
		else
		{
			return;
		}
		
		printf( "%s 0x%08X:%06X  Protect:%03X State:%s  %s\n",
			(shared.Type==MEM_IMAGE)?"Image  ":(shared.Type==MEM_MAPPED)?"Mapped ":(shared.Type==MEM_PRIVATE)?"Private":"Free   ",
			shared.AllocationBase, shared.RegionSize,
			shared.Protect,
			(shared.State==MEM_COMMIT)?"COMMIT ":(shared.State==MEM_FREE)?"FREE   ":(shared.State==MEM_RESERVE)?"RESERVE":"MIXED  ",
			bFileName ? szFileName : bIsDLL ? "Looks like a DLL" : "" );
	}
	bool IsSystemFile( const char* filename )
	{
		// Look at this awesome code!
		return strstr( filename, "Windows" )!=nullptr;
	}

private:
	bool ignoreSystem;
	bool showMemory;
};

void mainMemory( int argc, char* argv[] )
{
	CScanMemory ldr;
	ldr.Main( argc, argv );
}
