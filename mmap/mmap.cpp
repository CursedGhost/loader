// mmap.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "plsdk/manager.h"


void mainTest( );
void mainMap( int argc, char* argv[] );
void mainInject( int argc, char* argv[] );
void mainWinHooks( int argc, char* argv[] );
void mainEject( int argc, char* argv[] );
void mainStealth( int argc, char* argv[] );
void mainLaunch( int argc, char* argv[] );
void mainPatch( int argc, char* argv[] );
void mainMemory( int argc, char* argv[] );
void mainDump( int argc, char* argv[] );

void DestroyCmdLine()
{
	srand( static_cast<unsigned int>(__rdtsc()) );
	wchar_t* pszCmdLine = ::GetCommandLineW();
	size_t cbLen = wcslen( pszCmdLine );
	size_t cbCutOff = static_cast<size_t>(rand())%cbLen;
	for ( size_t i = 0; i<cbLen; ++i ) {
		pszCmdLine[i] = (i==cbCutOff) ? 0 : pszCmdLine[i]^rand();
	}
}

int main( int argc, char* argv[] )
{
	DestroyCmdLine();

	if ( argc>=2 )
	{
		const char* mode = argv[1];
		if ( !_stricmp( mode, "test" ) )
		{
			mainTest();
		}
		else if ( !_stricmp( mode, "map" ) )
		{
			mainMap( argc, argv );
		}
		else if ( !_stricmp( mode, "inject" ) )
		{
			mainInject( argc, argv );
		}
		else if ( !_stricmp( mode, "winhooks" ) )
		{
			mainWinHooks( argc, argv );
		}
		else if ( !_stricmp( mode, "eject" ) )
		{
			mainEject( argc, argv );
		}
		else if ( !_stricmp( mode, "stealth" ) )
		{
			mainStealth( argc, argv );
		}
		else if ( !_stricmp( mode, "launch" ) )
		{
			mainLaunch( argc, argv );
		}
		else if ( !_stricmp( mode, "patch" ) )
		{
			mainPatch( argc, argv );
		}
		else if ( !_stricmp( mode, "scan" ) )
		{
			mainMemory( argc, argv );
		}
		else if ( !_stricmp( mode, "detail" ) )
		{
			mainDump( argc, argv );
		}
		else
		{
			printf( "Invalid command '%s'!\n", mode );
		}
	}
	else
	{
		printf( "Dll injector by Casual_Hacker, see mmap.txt for documentation.\n"
			"mmap.exe <mode> [args]\n"
			);
	}
	return 0;
}

