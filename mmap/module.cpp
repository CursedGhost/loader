#include "stdafx.h"

#include <tools/printf.h>
#include <filesystem/path.h>
#include "module.h"

bool GetWin32Path( const wchar_t* input, filesystem::path& path )
{
	// Input has something like \Device\HarddiskVolume2\... which we want to get a C:\... from

	wchar_t volumeName[512];
	wchar_t deviceName[512];
	wchar_t driveLetter[64];

	HANDLE hList = ::FindFirstVolumeW( volumeName, sizeof(volumeName)/sizeof(volumeName[0]) );
	if ( hList!=INVALID_HANDLE_VALUE )
	{
		do
		{
			DWORD length;
			if ( ::GetVolumePathNamesForVolumeNameW( volumeName, driveLetter, sizeof(driveLetter)/sizeof(driveLetter[0]), &length )
				&& length>2 && driveLetter[1]==L':' )
			{
				driveLetter[2] = 0;
				if ( ::QueryDosDeviceW( driveLetter, deviceName, sizeof(deviceName)/sizeof(deviceName[0]) )>0 )
				{
					size_t len = wcslen(deviceName);
					if ( !memcmp( deviceName, input, len*sizeof(deviceName[0]) ) )
					{
						path.assign( driveLetter );
						path.append( input+len );
						::FindVolumeClose( hList );
						return true;
					}
				}
			}
		}
		while ( ::FindNextVolumeW( hList, volumeName, sizeof(volumeName) ) );
		::FindVolumeClose( hList );
	}
	return false;
}

CModule::CModule( HANDLE hProcess, const MEMORY_BASIC_INFORMATION& shared ) : hProcess(hProcess), shared(shared), copy(NULL)
{
}
CModule::~CModule()
{
	if ( copy ) {
		::VirtualFree( copy, 0, MEM_FREE );
		copy = NULL;
	}
}
bool CModule::Analyze( unsigned char* data, int threshold )
{
	IMAGE_DOS_HEADER* dos = (IMAGE_DOS_HEADER*) data;
	IMAGE_NT_HEADERS* nt = (IMAGE_NT_HEADERS*)( (uintptr_t)dos + dos->e_lfanew );
	IMAGE_SECTION_HEADER* sec;

	// Should be in a reasonable range
	bool e_lfanew = dos->e_lfanew>=0xA0 && dos->e_lfanew<=0x170 && (dos->e_lfanew&0x7)==0;

	// If the signatures match assume it's valid
	if ( dos->e_magic==IMAGE_DOS_SIGNATURE && e_lfanew && nt->Signature==IMAGE_NT_SIGNATURE )
	{
		sec = (IMAGE_SECTION_HEADER*)( (uintptr_t)&nt->OptionalHeader + nt->FileHeader.SizeOfOptionalHeader );
		return true;
	}

	// Ok we didn't find signatures, look at other members that are hard to hide
	// For now we'll stop here.. :)

	//for ( auto it = data, end = it+0x1000-sizeof("_runfunc@20"); it<end; ++it )
	//{
	//	if ( !memcmp( it, "_runfunc@20", sizeof("_runfunc@20") ) )
	//		return true;
	//}

	return false;
}

void CModule::DumpPE()
{
	ReadPE( true );
	FixupModule();
	FILE* h = GetOutputFile();
	WriteFile( h );
	::fclose( h );
}
void CModule::DumpRaw()
{
	if ( FILE* h = GetOutputFile() )
	{
		::fwrite( copy, 1, shared.RegionSize, h );
		::fclose( h );
	}
}
bool CModule::ReadPE( bool disk )
{
	// Just copy the PE headers (to avoid huge copies)
	if ( copy = ::VirtualAlloc( NULL, 0x1000, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE ) )
	{
		SIZE_T bytesRead;
		if ( ::ReadProcessMemory( hProcess, shared.AllocationBase, copy, 0x1000, &bytesRead ) )
		{
			// Read PE header and figure out file size
			dos = (IMAGE_DOS_HEADER*) copy;
			nt = (IMAGE_NT_HEADERS*)( (uintptr_t)copy + dos->e_lfanew );
			sec = (IMAGE_SECTION_HEADER*)( (uintptr_t)&nt->OptionalHeader + nt->FileHeader.SizeOfOptionalHeader );

			// Attempt to dump Loadout.exe which failed...
			//if ( disk )
			//{
			//	FILE* h;
			//	wchar_t fileName[512];
			//	filesystem::path path;
			//	if ( ::GetMappedFileNameW( hProcess, shared.AllocationBase, fileName, sizeof(fileName)/sizeof(wchar_t) )>0 &&
			//		GetWin32Path( fileName, path ) &&
			//		 ( h = ::fopen( path.c_str(), "rb" ) ) )
			//	{
			//		::fseek( h, (size_t)sec - (size_t)copy, FILE_BEGIN );
			//		::fread( sec, sizeof(IMAGE_SECTION_HEADER), nt->FileHeader.NumberOfSections, h );
			//		::fclose( h );
			//	}
			//	return false;
			//}

			return true;
		}
	}
	return false;
}
unsigned long CModule::GetFileSize()
{
	// File size is find the section with the highest raw address and add its size
	unsigned long fileSize = 0;
	for ( auto it = sec, end = sec + nt->FileHeader.NumberOfSections; it!=end; ++it )
	{
		if ( it->PointerToRawData + it->SizeOfRawData > fileSize )
		{
			fileSize = it->PointerToRawData + it->SizeOfRawData;
		}
	}
	return fileSize;
}
void CModule::FixupModule()
{
	//// Fix AddressOfEntryPoint
	//if ( (PVOID)nt->OptionalHeader.AddressOfEntryPoint>shared.AllocationBase )
	//{
	//	nt->OptionalHeader.AddressOfEntryPoint -= nt->OptionalHeader.ImageBase;
	//}
	// Fix reloactions
	FixRelocations();
}
void CModule::FixRelocations()
{
	if ( (PVOID)nt->OptionalHeader.ImageBase!=shared.AllocationBase )
	{
		// Was relocated, be lazy and just update ImageBase...
		nt->OptionalHeader.ImageBase = (DWORD)shared.AllocationBase;
	}
}
FILE* CModule::GetOutputFile()
{
	return ::fopen( tools::va_printf<64>( "m%08Xs%06X.bin", shared.AllocationBase, shared.RegionSize ), "wb" );
}
void CModule::WriteFile( FILE* h )
{
	// Write headers
	::fseek( h, 0, FILE_BEGIN );
	uintptr_t sizeHeader = (uintptr_t)(sec+nt->FileHeader.NumberOfSections) - (uintptr_t)dos;
	::fwrite( dos, 1, sizeHeader, h );

	unsigned char blob[0x1000];
	SIZE_T bytesRead;

	// Write sections
	for ( auto it = sec, end = sec + nt->FileHeader.NumberOfSections; it!=end; ++it )
	{
		if ( it->PointerToRawData )
		{
			::fseek( h, it->PointerToRawData, FILE_BEGIN );

			// Copy data from process
			for ( auto mptr = (unsigned char*)copy + it->VirtualAddress, mend = mptr + it->SizeOfRawData; mptr<mend; mptr += 0x1000 )
			{
				// Read memory and write all zeros if unavailable
				if ( !::ReadProcessMemory( hProcess, mptr, &blob, sizeof(blob), &bytesRead ) )
				{
					memset( &blob, 0, sizeof(blob) );
				}
				// Write to file
				size_t rem = mend - mptr;
				if ( rem>0x1000 ) rem = 0x1000;
				::fwrite( &blob, 1, rem, h );
			}
		}
	}
}
