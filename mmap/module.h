#pragma once



class CModule
{
public:
	CModule( HANDLE hProcess, const MEMORY_BASIC_INFORMATION& shared );
	~CModule();

	// Analyze the memory to detect if there's a module hidden in it.
	static bool LookForPeHeader();
	static bool Analyze( unsigned char* data, int threshold );

	void DumpPE();
	void DumpRaw();
	bool ReadPE( bool disk );
	unsigned long GetFileSize();
	void FixupModule();
	void FixRelocations();
	FILE* GetOutputFile();
	void WriteFile( FILE* h );

private:
	HANDLE hProcess;
	// Memory we're extracting the module from
	MEMORY_BASIC_INFORMATION shared;
	// Copy of the first memory page
	PVOID copy;

	IMAGE_DOS_HEADER* dos;
	IMAGE_NT_HEADERS* nt;
	IMAGE_SECTION_HEADER* sec;
};
