#include "stdafx.h"

#include "loader.h"
#include "idhook.h"
#include <pelite/lib.h>

class CIDHookPatch : public CIDHook
{
public:
	CIDHookPatch( pelite::PeFile& bin ) : bin(bin)
	{
	}

	virtual bool Read( const void* pAddress, void* pBuffer, size_t bytes )
	{
		return false;
	}
	virtual bool Write( void* pAddress, const void* pBuffer, size_t bytes )
	{
		return false;
	}
	virtual void* Allocate()
	{
		// Find space in the 
		return NULL;
	}

private:
	pelite::PeFile& bin;
};

class CPatchInject : public CBaseLoader
{
public:
	CPatchInject() : bUndo(false)
	{
	}
	virtual void Process( arg_t& arg )
	{
		// Interpret process argument as path to file we want to patch
		auto path = filesystem::make_path( arg.v[arg.i++] );

		// Read the file in memory
		//if ( binFile.Init( path.c_str() ) )
		{
			Action( arg );
			// Overwrite the original?
			// FIXME! Let us make a backup or tell user to make backup?
			Save( path );
		}
	}
	void Save( filesystem::path& path )
	{
	}
	virtual void PrintDescription()
	{
		printf( "Patch binaries to automatically load desired DLLs.\n"
			"Usage: mmap.exe patch [flags] <path/to/program.exe> [bin1.dll] [bin2.dll] ...\n"
			" --undo (-u) Remove the path.\n" );
	}
	virtual bool Argument( arg_t& arg )
	{
		char* s = arg.v[arg.i];
		// Launch a new process with these launch params
		if ( !_stricmp( s, "-u" ) || !_stricmp( s, "--undo" ) )
		{
			bUndo = true;
			return ++arg.i < arg.c;
		}
		return false;
	}
	virtual bool Attach( unsigned long pid, unsigned long tid, arg_t& arg )
	{
		assert( false );
		return false;
	}
	virtual void Action( arg_t& arg )
	{
		// at this point the file we want to patch is already opened & ready to be patched
		// here we do the actual patching

		CIDHookPatch hook( binFile );

	}

	bool bUndo;
	pelite::PeFileRaw binFile;
};

void mainPatch( int argc, char* argv[] )
{
	CPatchInject ldr;
	ldr.Main( argc, argv );
}
