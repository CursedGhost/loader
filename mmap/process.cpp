#include "stdafx.h"

#include "process.h"


bool FindModuleInSnapshot( HANDLE hSnap, const char* name, MODULEENTRY32& me )
{
	me.dwSize = sizeof(me);
	if ( ::Module32First( hSnap, &me ) )
	{
		do
		{
			if ( !::_stricmp( name, me.szModule ) )
			{
				return true;
			}
		}
		while ( ::Module32Next( hSnap, &me ) );
	}
	return false;
}
bool FindRemoteModule( int pid, const char* name, MODULEENTRY32& me )
{
	// Find the module name itself
	for ( const char* it = name; *it; ++it )
	{
		if ( *it=='/' || *it=='\\' )
			name = it+1;
	}

	bool s = false;
	HANDLE hSnap = ::CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, pid );
	if ( hSnap!=INVALID_HANDLE_VALUE )
	{
		s = FindModuleInSnapshot( hSnap, name, me );
		::CloseHandle( hSnap );
	}
	return s;
}


bool FindProcessInSnapshot( HANDLE hSnap, const char* name, PROCESSENTRY32& pe )
{
	pe.dwSize = sizeof(pe);
	if ( ::Process32First( hSnap, &pe ) )
	{
		do
		{
			if ( !::_stricmp( name, pe.szExeFile ) )
			{
				return true;
			}
		}
		while ( ::Process32Next( hSnap, &pe ) );
	}
	return false;
}
bool FindProcessByName( const char* name, PROCESSENTRY32& pe )
{
	bool s = false;
	HANDLE hSnap = ::CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, 0 );
	if ( hSnap!=INVALID_HANDLE_VALUE )
	{
		s = FindProcessInSnapshot( hSnap, name, pe );
		::CloseHandle( hSnap );
	}
	return s;
}
