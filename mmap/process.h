#pragma once

typedef struct tagMODULEENTRY32 MODULEENTRY32;
typedef struct tagPROCESSENTRY32 PROCESSENTRY32;

bool FindRemoteModule( int pid, const char* name, MODULEENTRY32& me );
bool FindProcessByName( const char* name, PROCESSENTRY32& pe );
