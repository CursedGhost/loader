// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#include "targetver.h"

// Windows includes
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Psapi.h>
#include <TlHelp32.h>
#include <WinNT.h>
#include <intrin.h>

// C headers
#include <cstdio>

#include <tchar.h>

#include <exception>
#include <string>

// Options...
#define PLSDK_HASDEBUG
#define PLSDK_XSAFESEH
