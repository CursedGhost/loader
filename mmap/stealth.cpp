#include "stdafx.h"

#include "loader.h"

class CROPChainBuilder
{
public:
	CROPChainBuilder() : it(1), prev(0) {}

	void Push1( void* pfn, uintptr_t arg1 )
	{
		// Overwrite return address in previous frame
		stack[prev] = (uintptr_t) pfn;
		stack[it] = 0;
		prev = it;
		// Put argument on stack
		stack[it+1] = arg1;
		// Advance stack
		it += 2;
		assert( it<(sizeof(stack)/sizeof(stack[0])) );
	}
	void Push2( void* pfn, uintptr_t arg1, uintptr_t arg2 )
	{
		// Overwrite return address in previous frame
		stack[prev] = (uintptr_t) pfn;
		stack[it] = 0;
		prev = it;
		// Put arguments on stack
		stack[it+1] = arg1;
		stack[it+2] = arg2;
		it += 3;
		assert( it<(sizeof(stack)/sizeof(stack[0])) );
	}

	unsigned int getStackSize()
	{
		return (it-1) * sizeof(uintptr_t);
	}
	uintptr_t* getStackPtr()
	{
		return stack+1;
	}
	void* getFirstFunction()
	{
		return (void*) stack[0];
	}

private:
	unsigned int it, prev;
	uintptr_t stack[64];
};

class CLoaderStealth : public CBaseLoader
{
public:
	typedef CBaseLoader BaseClass;
	CLoaderStealth()
	{
	}
	virtual void PrintDescription()
	{
		printf( "Stealthy DLL injector.\n"
			"Usage: mmap.exe stealth [flags] <process.exe> [bin1.dll] [bin2.dll] ...\n" );
		BaseClass::PrintDescription();
	}
	virtual void Action( arg_t& arg )
	{
		unsigned int numDlls = arg.c - arg.i;
		if ( numDlls<1 || numDlls>8 )
			return;

		// Prepare stackframes for ROP chain
		CROPChainBuilder chain;

		// Allocate memory for the file names
		void* pRemote = proc.Alloc( NULL, 0x1000, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE );
		void* pAlloc = pRemote;

		if ( !pRemote )
		{
			printf( "Failed to allocate memory!\n" );
			return;
		}

		// Fill in the stack frames for every dll to be injected
		for ( unsigned int i = 0; i<numDlls; ++i )
		{
			// Get the full path for this module
			filesystem::path path;
			GetFilePath( arg.v[arg.i+i], path );

			// Write module to remote process address space
			if ( proc.Write( pAlloc, path ) )
			{
				// Setup frame for this LoadLibraryA call
				chain.Push1( &LoadLibraryA, (uintptr_t)pAlloc );

				// Increment alloc for next paths
				pAlloc = (void*)( (uintptr_t)pAlloc + sizeof(path) );
			}
		}

		if ( HANDLE hThread = GetAnyThread() )
		{
			// Execute the ROP chain
			ExecROPChain( hThread, chain, pAlloc );

			// Wait until all DLLs are loaded (or timeout)
			for ( int i = 0; i<100 && !ConfirmInjection( arg, false ); ++i, Sleep( 50 ) );
			ConfirmInjection( arg, true );
		}

		// Cleanup
		proc.Free( pRemote, 0, MEM_FREE );
	}

	void GetFilePath( const char* s, filesystem::path& path )
	{
		auto file = filesystem::make_path(s);
		if ( file.is_absolute() )
		{
			path = file;
		}
		else
		{
			::GetCurrentDirectoryA( sizeof(path), path.buffer );
			path.append( file );
		}
	}

	void DebugContext( const CONTEXT& ctx )
	{
		filesystem::path path;
		::GetMappedFileNameA( proc.getHandle(), (LPVOID)ctx.Eip, path.c_str(), sizeof(path) );

		MEMORY_BASIC_INFORMATION inf;
		::VirtualQueryEx( proc.getHandle(), (LPVOID)ctx.Eip, &inf, sizeof(inf) );

		printf( " Hijacking at %s+%X\n", path.filename(), ctx.Eip - (DWORD)inf.AllocationBase );
	}

	void ExecROPChain( HANDLE hThread, CROPChainBuilder& chain, void* pAlloc )
	{
		DWORD ret;
		CONTEXT ctx, hack;

		// Thread needs to be suspended or we'll be working with stale data
		if ( ::SuspendThread( hThread )!=-1 ) {

		// Get the thread's context for hijacking and restoration
		ctx.ContextFlags = CONTEXT_ALL;
		if ( ::GetThreadContext( hThread, &ctx ) ) {
		hack = ctx;

		// Debugging...
		DebugContext( ctx );

		// Put in a final call to SetThreadContext( GetCurrentThread(), &ctx )
		if ( proc.Write( pAlloc, ctx ) ) {
		chain.Push2( &SetThreadContext, -2, (uintptr_t)pAlloc );

		// Write in ROP chain
		unsigned int fsize = chain.getStackSize();
		hack.Esp -= fsize + 256;
		if ( proc.Write( (LPVOID)hack.Esp, chain.getStackPtr(), fsize ) ) {

		// Hijack the thread
		hack.ContextFlags = CONTEXT_CONTROL;
		hack.Ebp = 0;
		hack.Eip = (DWORD) chain.getFirstFunction();
		if ( ::SetThreadContext( hThread, &hack ) ) {

		} else { printf( " Failed SetThreadContext() with 0x%x!\n", ::GetLastError() ); }
		} else { printf( " Failed writing the ROP chain with 0x%x!\n", ::GetLastError() ); }
		} else { printf( " Failed writing restore context with 0x%x!\n", ::GetLastError() ); }
		} else { printf( " Failed GetThreadContext() with 0x%x!\n", ::GetLastError() ); }

		// Run the thread, must be done even in case of error to avoid dead processes!
		ret = ::ResumeThread( hThread );

		} else { printf( " Failed SuspendThread() with 0x%x!\n", ::GetLastError() ); }
	}

	HANDLE GetAnyThread()
	{
		unsigned long tid = proc.getThreadID();

		// If no thread ID was provided, get one ourselves
		if ( !tid )
		{
			HANDLE hSnap = ::CreateToolhelp32Snapshot( TH32CS_SNAPTHREAD, proc.getProcessID() );
			if ( hSnap!=INVALID_HANDLE_VALUE )
			{
				THREADENTRY32 te;
				te.dwSize = sizeof(te);
				if ( ::Thread32First( hSnap, &te ) )
				{
					do
					{
						if ( te.th32OwnerProcessID==proc.getProcessID() )
						{
							tid = te.th32ThreadID;
							break;
						}
					}
					while ( ::Thread32Next( hSnap, &te ) );
				}
				::CloseHandle( hSnap );
			}
		}

		DWORD rights = SYNCHRONIZE | THREAD_GET_CONTEXT | THREAD_SET_CONTEXT | THREAD_SUSPEND_RESUME;
		HANDLE hThread = ::OpenThread( rights, FALSE, tid );
		if ( !hThread ) { printf( " Failed OpenThread() with 0x%x!\n", ::GetLastError() ); }
		return hThread;
	}

	bool ConfirmInjection( const arg_t& arg, bool talk )
	{
		unsigned int total = 0;
		unsigned int count = arg.c - arg.i;

		HANDLE hSnap = ::CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, proc.getProcessID() );
		if ( hSnap!=INVALID_HANDLE_VALUE )
		{
			MODULEENTRY32 me;
			me.dwSize = sizeof(me);
			for ( unsigned int i = 0; i<count; ++i )
			{
				const char* filename = filesystem::make_path(arg.v[arg.i+i]).filename();
				bool found = false;

				if ( ::Module32First( hSnap, &me ) )
				{
					do
					{
						if ( !_stricmp( filename, me.szModule ) )
						{
							found = true;
							break;
						}
					}
					while ( ::Module32Next( hSnap, &me ) );
				}

				total += found;
				if ( talk )
				{
					printf( found ? " Injected '%s' at 0x%08X\n" : " Failed to inject '%s'!\n", filename, me.hModule );
				}
			}
			::CloseHandle( hSnap );
		}
		return total==count;
	}
};

void mainStealth( int argc, char* argv[] )
{
	CLoaderStealth ldr;
	ldr.Main( argc, argv );
}
