#include "stdafx.h"

#include "plsdk/manager.h"
#include <filesystem/filecstd.h>
#include <tools/sehguard.h>
#include <hash/hash.h>

void mainTestManager()
{
	plsdk::Process proc;
	if ( proc.Attach() )
	{
	plsdk::Manager mgr( proc, true );

	filesystem::filecstd pl, bin;
	if ( pl.open( "..\\Debug\\payload.dll", filesystem::MODE_READ|filesystem::MODE_BINARY ) )
	{
		mgr.Q_Begin( &pl );

		mgr.Q_SetError( "Hello World!" );
		plsdk::Manager::Error err;
		mgr.Q_GetError( err );

		bin.open( "..\\Debug\\dummy.dll", filesystem::MODE_READ|filesystem::MODE_BINARY );
		void* dummy = mgr.Q_MapDll( &bin, 0, err );

		mgr.Q_Call( dummy, HASH("_ExportedFunc@4"), "Hello Export!", sizeof("Hello Export!"), err );

		mgr.Q_End();
	}
	}
}
void mainTestLocal()
{
	plsdk::LocalMM local( true );
	local.Init( nullptr, "..\\Debug\\payload.dll" );
	
	try
	{
		tools::SehGuard guard;

		filesystem::filecstd bin;
		bin.open( "..\\Debug\\dummy.dll", filesystem::MODE_READ|filesystem::MODE_BINARY );
		void* dummy = local.Exports.MapDllFile( &bin, 0 );

		meta::declpfn<long (__stdcall*)( const char* )> pfn( local.Exports.SymAddr( dummy, HASH("_ExportedFunc@4") ) );
		pfn( "Hello Export!" );
	}
	catch ( const std::exception& err )
	{
		printf( "Bad! %s\n", err.what() );
	}
}
//void mainTestInject()
//{
//	plsdk::Manager::InjectDllEx( "..\\Debug\\payload.dll", MAKEINTRESOURCE(18), "Hello World!" );
//}

void mainTest()
{
	mainTestLocal();
	mainTestManager();
}
