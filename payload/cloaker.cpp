#include "stdafx.h"

#include "cloaker.h"
#include "syscall.h"

void PLAPI CloakModule( void* hmod )
{
	// TODO?
}

void PLAPI DestroyInitCode( unsigned char* begin )
{
	unsigned char* end = (unsigned char*) _ReturnAddress();

	// Make memory writable (remember we're modifying executable memory!)
	PVOID BaseAddress = begin;
	ULONG dwSize = end - begin;
	ULONG dwOldProtect;
	NTSTATUS nts;
	if ( !NT_SUCCESS(nts=SystemCall( kNtProtectVirtualMemory, CURRENT_PROCESS_HANDLE, &BaseAddress, &dwSize, PAGE_EXECUTE_READWRITE, &dwOldProtect )) )
		return;

	unsigned int s = (unsigned int)begin + (unsigned int)end, a = 1103515245, c = 12345;
	for ( unsigned char* it = begin; it!=end; ++it, s = s*a+c )
	{
		unsigned char r = (s>>24)&0xFF;
		*it ^= r;
	}

	// Restore protection flags
	nts=SystemCall( kNtProtectVirtualMemory, CURRENT_PROCESS_HANDLE, &BaseAddress, &dwSize, dwOldProtect, &dwOldProtect );
}
