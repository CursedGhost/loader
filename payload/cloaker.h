#pragma once

//----------------------------------------------------------------
// Cloaker
//----------------------------------------------------------------
// Cloak a standard LoadLibrary'd module.

#include "export.h"

PLEXPORT void PLAPI CloakModule( void* hmod );

PLEXPORT void PLAPI DestroyInitCode( unsigned char* begin );
