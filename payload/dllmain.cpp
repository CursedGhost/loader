// dllmain.cpp : Defines the entry point for the DLL application.

#include "stdafx.h"

#include "imports.h"
#include "manualmap.h"
#include "hybrid.h"
#include "syscall.h"
#include "payload.h"
#include "utility.h"


bool Init( void* hmod );
void Shutdown();

BOOL APIENTRY DllMain( HMODULE hModule, DWORD dwReason, LPVOID lpData )
{
	if ( dwReason==DLL_PROCESS_ATTACH )
	{
		return Init( hModule )!=FALSE;
	}
	else if ( dwReason==DLL_PROCESS_DETACH )
	{
		Shutdown();
	}
	return TRUE;
}


//
// Try to disable DEP because it allows is to execute without the execute flag.
// SafeSEH is a bitch.
// Currently unused since I'm relying on hybrid mode (hybrid.h/cpp)
//
bool _has_dep;
bool DisableDEP()
{
	if ( Imports.GetProcessDEPPolicy )
	{
		DWORD flags;
		BOOL perm;
		if ( !Imports.GetProcessDEPPolicy( (HANDLE)-1, &flags, &perm ) || (flags&PROCESS_DEP_ENABLE) )
		{
			if ( perm || !Imports.SetProcessDEPPolicy( 0 ) )
			{
				_has_dep = true;
			}
		}
	}
	return _has_dep;
}

bool Init( void* hmod )
{
	//
	// VERY experimental stuff!
	// The manual mapper for another process only does MINIMAL work and let's the payload initialize itself mostly!
	// This code assumes that if we're manual mapped relocs haven't been fixed yet.
	// To prevent loading at the preferred load address, I've forced the linker to make it 0x00000000.
	//
	// NOTE! Currently in permanent hybrid mode so this is unused!
	//
	CManualMap mmap;
	if ( !mmap.InitDllMain( hmod ) )
		return false;
	_has_dep = true;

	// Init pooled rwe memory
	if ( !rweInit( hmod ) )
		return false;

	// Init direct system call subsystem
	if ( !SystemInit() )
		return false;

	// Resolve our own hidden imports
	if ( !Imports.Init() )
		return false;

	// Init our cache
	PlInit( hmod, (EntryPointFn) &DllMain );

	return true;
}

void Shutdown()
{
}
