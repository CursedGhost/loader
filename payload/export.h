#pragma once

//----------------------------------------------------------------
// Exports
//----------------------------------------------------------------
//
// All export are by ordinal defined in payload.def
//

#include <cstdarg>

#define PLEXPORT extern "C" __declspec(noinline)
#define PLAPI __stdcall
