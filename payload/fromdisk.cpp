#include "stdafx.h"

#include <filesystem/file.h>
#include "fromdisk.h"
#include "imports.h"
#include "log.h"
#include "utility.h"
#include "payload.h"


class CDiskFile : public filesystem::file
{
public:
	explicit CDiskFile( HANDLE hFile = INVALID_HANDLE_VALUE ) : hFile(hFile) {
	}
	void operator delete( void* p ) { }

	// Inherited from filesystem::file
	virtual bool open( filesystem::hpath file, int mode ) {
		if ( hFile!=INVALID_HANDLE_VALUE )
			close();
		hFile = Imports.CreateFileA( file->c_str(), GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
		return hFile!=INVALID_HANDLE_VALUE;
	}
	virtual void close() {
		Imports.CloseHandle( hFile );
		hFile = INVALID_HANDLE_VALUE;
	}
	virtual int status() const { return 0; }
	virtual bool info( info_t& fi ) const { return false; }
	virtual size_t write( const void* src, size_t size ) { return 0; }
	virtual size_t vprintf( const char* fmt, va_list va = nullptr ) { return 0; }
	virtual void flush() { }

	virtual bool seek( pos_t offset, int origin ) {
		LARGE_INTEGER* li = (LARGE_INTEGER*)&offset;
		return Imports.SetFilePointer( hFile, li->LowPart, &li->HighPart, origin )!=-1;
	}
	virtual pos_t tell() const {
		return 0;
	}
	virtual size_t read( void* buf, size_t num, unsigned int term = -1 ) const {
		DWORD bytesRead;
		return Imports.ReadFile( hFile, buf, num, &bytesRead, NULL ) ? bytesRead : -1;
	}
	
private:
	HANDLE hFile;
};


PLEXPORT void* PLAPI PlLoadDllA( const char* dllname )
{
	ErrorClear();

	// Look in our own cache

	if ( module_t* it = PlLookupDll( WrapDllName( dllname ) ) )
	{
		++it->refc;
		LOG_ERROR( 0, "LoadDllA() Dll \"%s\" already loaded at 0x%08X", "%s!%08X", dllname, it->address );
		return it->address;
	}

	// ManualMap the dll if not found

	void* hmod = nullptr;

	CDiskFile ffd;
	if ( ffd.open( dllname, 0 ) )
	{
		hmod = PlMapDllFile( &ffd, 0 );
	}
	else
	{
		// Fallback to LoadLibrary (for system dlls etc)
		if ( !( hmod = Imports.LoadLibraryA( dllname ) ) )
		{
			LOG_ERROR( 0x12, "LoadDllA() Dll \"%s\" failed with %X", "%s>%x", dllname, 0 );
		}
	}

	if ( hmod )
	{
		LOG_ERROR( 0, "LoadDllA() Loaded \"%s\" at 0x%08X", "%s!%08X", dllname, hmod );
	}

	return hmod;
}
PLEXPORT void* PLAPI PlLoadDllW( const wchar_t* dllname )
{
	ErrorClear();

	// Look in our own cache
	
	if ( module_t* it = PlLookupDll( WrapDllName( dllname ) ) )
	{
		++it->refc;
		LOG_ERROR( 0, "LoadDllA() Dll \"%s\" already loaded at 0x%08X", "%s!%08X", dllname, it->address );
		return it->address;
	}
	
	// ManualMap the dll if not found

	void* hmod = nullptr;

	HANDLE hFile = Imports.CreateFileW( dllname, GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
	if ( hFile!=INVALID_HANDLE_VALUE )
	{
		CDiskFile ffd( hFile );
		hmod = PlMapDllFile( &ffd, 0 );
	}
	else
	{
		// Fallback to LoadLibrary (for system dlls etc)
		if ( !( hmod = Imports.LoadLibraryW( dllname ) ) )
		{
			LOG_ERROR( 0x1A, "LoadDllW() Dll \"%s\" failed with %x", "%S>%x", dllname, 0 );
		}
	}

	if ( hmod )
	{
		LOG_ERROR( 0, "LoadDllW() Loaded \"%S\" at 0x%08X", "%S!%08X", dllname, hmod );
	}
	
	return hmod;
}
