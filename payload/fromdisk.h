#pragma once

#include "export.h"

// These are needed for loading system dll files as needed
PLEXPORT void* PLAPI PlLoadDllA( const char* dllname );
PLEXPORT void* PLAPI PlLoadDllW( const wchar_t* dllname );
