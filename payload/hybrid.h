#pragma once

//----------------------------------------------------------------
// SafeSEH manual mapping solution
//----------------------------------------------------------------
//
//  Problem:
// Exception handlers are checked to ensure they're contained in a valid dll (regardless if the memory is executable).
// This protection cannot be disabled whenever we feel like it so a different solution is required.
//
//  Solution:
// When a process has SafeSEH enabled, this payload is LoadLibrary'd (as opposed to manual mapped).
// It contains an extra section a few MB large. When another module is mapped, it allocates memory from this pool for that module's code section.
// (so the module is 'cut' in pieces with the main sections allocated as usual with their code sections moved here).
// As a bonus I get thread creation / closing events for ez TLS support.
// The payload loader will ignore this section if manual mapped (because it's quite large and unneeded in that case).
//
//  Old:
// Create a temp dll and LoadLibrary that one (as opposed to the whole payload).
// Didn't work because I couldn't get it to generate a Dll LoadLibrary would accept :P
//
//  Notes:
// Use the shift tool to generate the pool section.
// Currently we defer all allocation to toolkit's rwe allocator.
//

#include <toolkit/alloc.h>

using toolkit::rweInit;
using toolkit::rweAlloc;
using toolkit::rweAligned;
using toolkit::rweFree;
using toolkit::rweValidate;

