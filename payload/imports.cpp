#include "stdafx.h"

#include "imports.h"
#include "utility.h"

CCustomImports Imports;

bool CCustomImports::Init()
{
	if ( void* hmod = PlModuleHandle( 0x3e003875/*kernel32.dll*/ ) )
	{
		TerminateProcess.pfn = PlSymAddr( hmod, 0x8cc082f7/*TerminateProcess*/ );

		TlsAlloc.pfn = PlSymAddr( hmod, 0xda43d6c3/*TlsAlloc*/ );
		TlsFree.pfn = PlSymAddr( hmod, 0xdfcf0a7a/*TlsFree*/ );

		CreateFileA.pfn = PlSymAddr( hmod, 0xcdf70c26/*CreateFileA*/ );
		CreateFileW.pfn = PlSymAddr( hmod, 0xcdf70c30/*CreateFileW*/ );
		DeleteFileA.pfn = PlSymAddr( hmod, 0x7216dc1b/*DeleteFileA*/ );
		SetFilePointer.pfn = PlSymAddr( hmod, 0x111b7d9a/*SetFilePointer*/ );
		ReadFile.pfn = PlSymAddr( hmod, 0x245d06b1/*ReadFile*/ );
		WriteFile.pfn = PlSymAddr( hmod, 0xde34165e/*WriteFile*/ );

		CloseHandle.pfn = PlSymAddr( hmod, 0x687c0d79/*CloseHandle*/ );
		
		LoadLibraryA.pfn = PlSymAddr( hmod, 0x01ed9add/*LoadLibraryA*/ );
		LoadLibraryW.pfn = PlSymAddr( hmod, 0x01ed9acb/*LoadLibraryW*/ );
		//FreeLibrary.pfn = PlSymAddr( hmod, 0x809469ce/*FreeLibrary*/ );

		//SetUnhandledExceptionFilter.pfn = PlSymAddr( hmod, 0xbfe15565/*SetUnhandledExceptionFilter*/ );

		GetProcessDEPPolicy.pfn = PlSymAddr( hmod, 0xc017e789/*GetProcessDEPPolicy*/ );
		SetProcessDEPPolicy.pfn = PlSymAddr( hmod, 0x63df8b9d/*SetProcessDEPPolicy*/ );

		OutputDebugStringA.pfn = PlSymAddr( hmod, 0x14cd3dff/*OutputDebugStringA*/ );
		OutputDebugStringW.pfn = PlSymAddr( hmod, 0x14cd3de9/*OutputDebugStringW*/ );
	}
	if ( void* hmod = PlModuleHandle( 0xe91aad51/*ntdll.dll*/ ) )
	{
		vsnprintf.pfn = PlSymAddr( hmod, 0x295be586/*_vsnprintf*/ );
		vsnwprintf.pfn = PlSymAddr( hmod, 0x43168ed1/*_vsnwprintf*/ );
	}

	return true;
}

