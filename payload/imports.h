#pragma once

//----------------------------------------------------------------
// Custom import table
//----------------------------------------------------------------
//
// This module will have no import directory (but it will have some imports we'll be fetching manually...)
//

#include <WinDef.h>
#include <meta/declpfn.h>

using meta::declpfn;


struct CCustomImports
{
	bool Init();

	// KERNEL32.DLL

	declpfn<BOOL (WINAPI*)( HANDLE hProcess, UINT uExitCode )>
		TerminateProcess;

	declpfn<DWORD (WINAPI*)( VOID )>
		TlsAlloc;
	declpfn<BOOL (WINAPI*)( DWORD dwTlsIndex )>
		TlsFree;

	declpfn<HANDLE (WINAPI*)( LPCSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, struct _SECURITY_ATTRIBUTES* lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile )>
		CreateFileA;
	declpfn<HANDLE (WINAPI*)( LPCWSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, struct _SECURITY_ATTRIBUTES* lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile )>
		CreateFileW;
	declpfn<BOOL (WINAPI*)( LPCSTR lpFileName )>
		DeleteFileA;
	declpfn<DWORD (WINAPI*)( HANDLE hFile, LONG lDistanceToMove, PLONG lpDistanceToMoveHigh, DWORD dwMoveMethod )>
		SetFilePointer;
	declpfn<BOOL (WINAPI*)( HANDLE hFile, LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead, struct _OVERLAPPED* lpOverlapped )>
		ReadFile;
	declpfn<BOOL (WINAPI*)( HANDLE hFile, LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten, struct LPOVERLAPPED* lpOverlapped )>
		WriteFile;

	declpfn<BOOL (WINAPI*)( HANDLE hObject )>
		CloseHandle;
	
	declpfn<HMODULE (WINAPI*)( LPCSTR lpModuleName )>
		LoadLibraryA;
	declpfn<HMODULE (WINAPI*)( LPCWSTR lpModuleName )>
		LoadLibraryW;
	//declpfn<BOOL (WINAPI*)( HMODULE hModule )>
	//	FreeLibrary;

	//declpfn<LPTOP_LEVEL_EXCEPTION_FILTER (WINAPI*)( LPTOP_LEVEL_EXCEPTION_FILTER lpTopLevelExceptionFilter )>
	//	SetUnhandledExceptionFilter;

	declpfn<BOOL (WINAPI*)( HANDLE hProcess, LPDWORD lpFlags, PBOOL lpPermanent )>
		GetProcessDEPPolicy;
	declpfn<BOOL (WINAPI*)( DWORD dwFlags )>
		SetProcessDEPPolicy;

	declpfn<VOID (WINAPI*)( LPCSTR lpOutputString )>
		OutputDebugStringA;
	declpfn<VOID (WINAPI*)( LPCWSTR lpOutputString )>
		OutputDebugStringW;

	// NTDLL.DLL

	declpfn<int (__cdecl*)( char* DstBuf, size_t MaxCount, const char* Format, va_list ArgList )>
		vsnprintf;
	declpfn<int (__cdecl*)( wchar_t* Dest, size_t Count, const wchar_t* Format, va_list Args )>
		vsnwprintf;
};

extern CCustomImports Imports;
