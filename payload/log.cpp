#include "stdafx.h"

#include "log.h"
#include "imports.h"

// Yeah this is currently based on global variables...
// NOT thread safe! For now I don't need that...

struct error_t
{
	int code;
	char desc[252];
} Error;


int ErrorClear()
{
	int err = Error.code;
	Error.code = 0;
	Error.desc[0] = 0;
	return err;
}
PLEXPORT int PLAPI ErrorGet()
{
	return Error.code;
}
PLEXPORT const char* PLAPI ErrorDesc()
{
	return Error.desc;
}
PLEXPORT int PLAPI ErrorSetEx( int code, const char* fmt, va_list va )
{
	Error.code = code;
	if ( fmt )
	{
		if ( !va )
		{
			va = (va_list)&fmt;
			fmt = STRDEF("%s");
		}
		Imports.vsnprintf( Error.desc, sizeof(Error.desc), fmt, va );

		// Convenience when debugging
		Imports.OutputDebugStringA( Error.desc );
		Imports.OutputDebugStringA( STRDEF("\n") );
	}
	else
	{
		(int&)Error.desc = 0;
	}
	return code;
}
