#pragma once

//----------------------------------------------------------------
// Primitive logging facility
//----------------------------------------------------------------
//
// Error code ranges...
// 0x00:0x0F dllmain.cpp
// 0x10:0x1F payload.cpp
// 0x20:0x3F manualmap.cpp
// 0x40:0x4F utility.cpp
//

#include <meta/strcrypt.h>
#include "export.h"

extern struct error_t Error;

// Clear the current error
int ErrorClear();
// Get the error code
PLEXPORT int PLAPI ErrorGet();
// Get the error description
PLEXPORT const char* PLAPI ErrorDesc();
// Set the error and description, formatting is optional
PLEXPORT int PLAPI ErrorSetEx( int code, const char* fmt, va_list va = nullptr );
inline int ErrorSet( int code, const char* fmt, ... )
{
	va_list va;
	va_start( va, fmt );
	return ErrorSetEx( code, fmt, va );
}

#ifdef _DEBUG
#define LOG_ERROR( code, debug, desc, ... ) ErrorSet( code, debug, __VA_ARGS__ )
#else
#define LOG_ERROR( code, debug, desc, ... ) ErrorSet( code, STRDEF(desc), __VA_ARGS__ )
#endif
