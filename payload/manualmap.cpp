#include "stdafx.h"

#include "manualmap.h"
#include "log.h"
#include "hybrid.h"
#include "utility.h"
#include "imports.h"
#include "payload.h"

using namespace pelite;


void* CManualMap::MapDllFile( filesystem::file* bin, unsigned flags )
{
	ErrorClear();
	bin->rewind();

	ImageDosHeader dos;
	ImageNtHeaders nt;

	// Read the dos header
	if ( bin->read( &dos, sizeof(dos) )!=sizeof(dos) || dos.e_magic!=ImageDosHeaderMagic )
	{
invalid:
		LOG_ERROR( 0x21, "CManualMap::MapDllFile() Error/Invalid!", "%x", flags );
		return nullptr;
	}

	// Read the nt header
	if ( !bin->seek( dos.e_lfanew, 0 ) || bin->read( &nt, sizeof(nt) )!=sizeof(nt) || nt.Signature!=ImageNtHeaderSignature )
		goto invalid;

	// Allocate memory
	void* hmod = Allocate( (void*)nt.OptionalHeader.ImageBase, nt.OptionalHeader.SizeOfImage, flags );

	if ( hmod )
	{
		// Map the sections
		module = hmod;
		LoadFromFile( bin, nt.OptionalHeader.SizeOfHeaders );
		myname = GrabDllName();

		// Initialize the image
		if ( !MapDllPart2( flags ) )
		{
			Free( hmod );
			hmod = nullptr;
		}
	}
	else
	{
		LOG_ERROR( 0x23, "CManualMap::MapDllFile() Failed to malloc!", "%x;%x", nt.OptionalHeader.SizeOfImage, flags );
	}

	return hmod;
}
bool CManualMap::UnmapDll( void* hmod, EntryPointFn pfn )
{
	ErrorClear();

	// Notify the dll about unloading

	if ( pfn )
	{
		int code = pfn( hmod, DLL_PROCESS_DETACH, NULL );
		if ( !code )
		{
			LOG_ERROR( 0x22, "ManualMap::UnmapDll() Detach notification failure!", "%x", code );
		}
	}

	// Free TLS slot

	// Free all dependent dlls

	return false;
}

void* CManualMap::Allocate( void* desired, unsigned long size, unsigned int flags )
{
	// If we can skip page protection, then all we need is 16 byte alignment
	// Otherwise fall back to 4096 byte alingment
	if ( !(flags&MMFL_SKIP_PROTECT) )
	{
		size |= (12<<28); // 4096 byte alignment
	}
	else
	{
		size |= (4<<28); // 16 byte alignment
	}
	return rweAlloc( size );
}
void CManualMap::Free( void* hmod )
{
	rweFree( hmod );
}


void CManualMap::LoadFromFile( filesystem::file* bin, unsigned int hdrsize )
{
	// Read the headers
	bin->rewind();
	bin->read( module, hdrsize );
	
	// Fetch headers
	nthdr = (ImageNtHeaders*)( ((uintptr_t)module) + ((ImageDosHeader*)module)->e_lfanew );
	shdr = (ImageSectionHeader*)( ((uintptr_t)&nthdr->OptionalHeader) + nthdr->FileHeader.SizeOfOptionalHeader );

	// Write all the sections themselves
	for ( auto it = shdr, end = it + nthdr->FileHeader.NumberOfSections; it<end; ++it )
	{
		// Some sections don't have any data and are just initialized to zero
		if ( it->PointerToRawData && it->VirtualAddress )
		{
			bin->seek( it->PointerToRawData, 0 );
			unsigned long* dest = MakePtr<unsigned long*>( it->VirtualAddress );
			bin->read( dest, it->SizeOfRawData );
		}
	}
}

unsigned int CManualMap::GrabDllName()
{
	// Grab the dllname from the exports dir if there is one
	
	unsigned int name = 0;
	ImageDataDirectory& dir = nthdr->OptionalHeader.DataDirectory[ DataDir_Export ];
	if ( dir.VirtualAddress )
	{
		ImageExportDirectory* exp = MakePtr<ImageExportDirectory*>( dir.VirtualAddress );
		const char* dllname = MakePtr<const char*>( exp->Name );
		name = WrapDllName( dllname );
	}
	return name;
}


__declspec(noinline) bool CManualMap::MapDllPart2( unsigned int flags )
{
	if ( !FixRelocs( (uintptr_t)module - nthdr->OptionalHeader.ImageBase ) )
		return false;
	
	if ( !(flags&MMFL_RESOURCEDLL) )
	{
		// Add this module to the cache already (so cyclic dependencies can see each other)
		// If loading fails it'll get removed again.
		// FIXME! LookupImports might load a dependent dll, if loading that one succeeds but this fails (InitModule()) the wrong module will be removed!!!
		// How should I handle this case?!?

		//Payload::g.Push( Module, myname );

		// Lookup the imports

		if ( !LookupImports() )
			goto loadfailed;

		// Finalize

		if ( flags&MMFL_KEEP_INTACT )
			ApplyProtect();
		else
			Finalize( flags );

		// FIXME!
		// Doesn't unload the module properly in case of failure...
		
		if ( !InitModule() )
			goto loadfailed;
	}
	return true;
	
loadfailed:
	// If loading fails we need to remove ourselves from the cache

	//Payload::g.Pop();
	return false;
}

bool CManualMap::FixRelocs( dword based )
{
	if ( based )
	{
		// If the delta isn't 0 we MUST have relocation data

		const ImageDataDirectory& dir = nthdr->OptionalHeader.DataDirectory[ DataDir_BaseReloc ];
		if ( !dir.VirtualAddress )
		{
			LOG_ERROR( 0x26, "CManualMap::FixRelocs() Missing relocation data, delta=%x!", "%x", based );
			return false;
		}

		// Iterate through all the relocation blocks

		typedef ImageBaseRelocation* iterator;
		for ( iterator it = MakePtr<iterator>( dir.VirtualAddress ), end = MakePtr<iterator>( it, dir.Size );
			it<end; it = MakePtr<iterator>( it, it->SizeOfBlock ) )
		{
			// Each of the next entries is an offset from this base
			// Check if source falls within moved section

			// Source
			dword src = it->VirtualAddress;
			dword* rbase = MakePtr<dword*>( src );

			for ( const ImageBaseRelocBlock* in = (ImageBaseRelocBlock*)(it+1),
				*en = MakePtr<ImageBaseRelocBlock*>( it, it->SizeOfBlock ); in<en; ++in )
			{
				switch ( in->Type )
				{
				default:
					// Error unknown reloc type...
					//__debugbreak();
					//return false;
				case ImageRelBasedAbsolute:
					break;
				case ImageRelBasedHighLow:
					*MakePtr<dword*>( rbase, in->Offset ) += based;
					break;
				}
			}
		}
	}
	return true;
}
bool CManualMap::LookupImports()
{
	// If this directory is missing there are no imports and we don't need to do anything
	ImageDataDirectory& dir = nthdr->OptionalHeader.DataDirectory[ DataDir_Import ];
	if ( !dir.VirtualAddress ) return true;

	// Get import descriptor
	ImageImportDescriptor* desc = MakePtr<ImageImportDescriptor*>( dir.VirtualAddress );
	return LookupImports( desc );
}
bool CManualMap::LookupImports( const ImageImportDescriptor* desc )
{
	for ( ; desc->OriginalFirstThunk; ++desc )
	{
		// Module to import from

		const char* dllname = MakePtr<const char*>( desc->Name );
		void* hmod = PlLoadDllA( dllname );

		if ( !hmod || !ImportsFor( desc, hmod ) )
		{
			// Failed to load module...
			assert( ErrorGet() );
			return false;
		}
	}
	return true;
}
bool CManualMap::ImportsFor( const ImageImportDescriptor* desc, void* hmod )
{
	const char* dllname = MakePtr<const char*>( desc->Name );

	// The module we're importing from MUST have an export directory!
	
	const ImageNtHeaders* nt = MakePtr<ImageNtHeaders*>( hmod, ((ImageDosHeader*)hmod)->e_lfanew );
	const ImageDataDirectory& dir = nt->OptionalHeader.DataDirectory[ DataDir_Export ];
	if ( !dir.VirtualAddress )
	{
		LOG_ERROR( 0x31, "CManualMap::ImportsFor() Missing exports directory for \"%s\"!", "%s", dllname );
		return false;
	}
	const ImageExportDirectory* exp = MakePtr<ImageExportDirectory*>( hmod, dir.VirtualAddress );

	// Lookup every import from this module
	
	void** iat = MakePtr<void**>( desc->FirstThunk );

	for ( ImageImportThunk* thk = MakePtr<ImageImportThunk*>( desc->OriginalFirstThunk ); thk->AddressOfData; ++thk, ++iat )
	{
		// Imported by ordinal
		if ( thk->Ordinal<0 )
		{
			int index, sym = 0, ord = thk->Ordinal&0xFFFF;
			if ( static_cast<dword>( index = ord - exp->Base )<exp->NumberOfFunctions &&
				( sym = MakePtr<unsigned int*>( hmod, exp->AddressOfFunctions )[index] ) )
			{
				*iat = MakePtr<void*>( hmod, sym );
			}
			else
			{
				LOG_ERROR( 0x32, "CManualMap::ImportsFor() Invalid ordinal %d from \"%s\"!", "%d;%s", ord, dllname );
				return false;
			}

		}
		// Imported by name
		else
		{
			const ImageImportByName* p = MakePtr<ImageImportByName*>( thk->AddressOfData );

			void* addr = PlSymAddrA( hmod, p->Name );
			if ( !addr )
			{
				LOG_ERROR( 0x32, "CManualMap::ImportsFor() Symbol \"%s\" not found in \"%s\"!", "%s;%s", p->Name, dllname );
				return false;
			}
			*iat = addr;
		}
	}

	return true;
}

void CManualMap::ApplyProtect()
{

}
bool CManualMap::InitModule()
{
	// Allocate a TLS slot

	const ImageDataDirectory& dir = nthdr->OptionalHeader.DataDirectory[ DataDir_TLS ];
	if ( dir.VirtualAddress )
	{
		ImageTlsDirectory* tls = MakePtr<ImageTlsDirectory*>( dir.VirtualAddress );
		dword* slot = (dword*) tls->AddressOfIndex;
		*slot = Imports.TlsAlloc();
	}

	// Call entry point
	// If this fails we have to unload the module as normal 

	EntryPointFn pfn = nullptr;
	if ( dword ep = nthdr->OptionalHeader.AddressOfEntryPoint )
	{
		pfn = MakePtr<EntryPointFn>( ep );
	}

	PlPushDll( module, pfn, myname );

	if ( pfn && !pfn( module, DLL_PROCESS_ATTACH, NULL ) )
	{
		LOG_ERROR( 0x27, "CManualMap::InitModule() Failure!", "" );
		return false;
	}
	return true;
}


void CManualMap::Finalize( unsigned int flags )
{
	// Clear out BaseRelocs, Imports and Debug directories
	
	ClearDataDir( DataDir_BaseReloc );
	ClearDataDir( DataDir_Import );
	ClearDataDir( DataDir_Debug );
	
	// Remove all non essentials from exports

	{ImageDataDirectory& dir = nthdr->OptionalHeader.DataDirectory[ DataDir_Export ];
	if ( dir.VirtualAddress )
	{
		ImageExportDirectory* exp = MakePtr<ImageExportDirectory*>( dir.VirtualAddress );
		exp->TimeDateStamp = 0;
		exp->Characteristics = 0;
		exp->MajorVersion = 0;
		exp->MinorVersion = 0;
		for ( char* s = MakePtr<char*>( exp->Name ); *s; s++ )
			*s = 0;
		exp->Name = 0;
	}}
	
	// Must be done before stripping the section headers

	if ( !(flags&MMFL_SKIP_PROTECT) )
		ApplyProtect();

	// Strip away at the PE headers
	
	unsigned long lfanew = ((ImageDosHeader*)module)->e_lfanew;
	if ( flags&MMFL_STRIP_PEHEADERS )
	{
		// FIXME! The code below reads/writes from the WRONG headers!!!
		ImageNtHeaders* nthdr = MakePtr<ImageNtHeaders*>( lfanew );
		ImageSectionHeader* shdr = MakePtr<ImageSectionHeader*>( &nthdr->OptionalHeader, nthdr->FileHeader.SizeOfOptionalHeader );

		// Clear out the section headers
		__stosd( (unsigned long*)shdr, 0, nthdr->FileHeader.NumberOfSections*sizeof(ImageSectionHeader)/4 );
		// Clear out all until DataDirectory
		__stosd( (unsigned long*)module, 0, ((unsigned long*)&nthdr->OptionalHeader.DataDirectory) - ((unsigned long*)module) );

	}
	else
	{
		// Clear out the dos header and signatures

		__stosd( (unsigned long*)module, 0, lfanew/4 );

		nthdr->Signature = 0;
		nthdr->OptionalHeader.Magic = 0;
	}
	((ImageDosHeader*)module)->e_lfanew = lfanew;
}
void CManualMap::ClearDataDir( unsigned int i )
{
	ImageDataDirectory& dir = nthdr->OptionalHeader.DataDirectory[ i ];
	if ( dir.VirtualAddress )
	{
		__stosd( MakePtr<unsigned long*>( dir.VirtualAddress ), 0, dir.Size/4 );
		dir.VirtualAddress = 0;
		dir.Size = 0;
	}
}


extern "C" BOOL __stdcall DllMain( HMODULE hModule, DWORD dwReason, LPVOID lpData );
bool CManualMap::InitDllMain( void* hmod )
{
	module = hmod;
	nthdr = MakePtr<ImageNtHeaders*>( hmod, ((ImageDosHeader*)hmod)->e_lfanew );
	shdr = MakePtr<ImageSectionHeader*>( &nthdr->OptionalHeader, nthdr->FileHeader.SizeOfOptionalHeader );
	myname = 0;

	// OH GOD since I'm not overriding the entry point in debug builds this is different...
	// I cannot link against the real entry point... So just assume everything's fine...
#ifdef _DEBUG
	if ( true )
#else
	if ( MakePtr<void*>( hmod, nthdr->OptionalHeader.AddressOfEntryPoint )==&DllMain )
#endif
	{
		// Since we've set our base address to zero we'll always be relocated
		// If we reach this, we have been relocated and can assume we're already ready to go!
		return true;
	}
	else
	{
		// Loaded did a bare minimum of work (didn't fix our relocs) so do it now
		return FixRelocs( (dword)hmod );
	}
}
