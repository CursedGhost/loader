#pragma once

//----------------------------------------------------------------
// Payload's manual mapping facility
//----------------------------------------------------------------
//
// Error codes:
//  0x21 - Error reading / Not a PE file
//  0x22 - Detach notification failure
//  0x23 - Allocation failure
//  0x24 - No name found, will not be visible to other modules
//
//  0x25 - MapDllPart2 Failed
//  0x26 - Missing relocation directory
//  0x27 - Failed to initialize the module
//
//  0x30 - Importing failed, either the required dll failed to load or an import remained unresolved
//  0x31 - Dll being imported from is lacking an export directory
//  0x32 - Imported symbol not found
//

#include <pelite/image.h>
#include <filesystem/file.h>
#include "export.h"

typedef int (__stdcall* EntryPointFn)( void*, unsigned int, void* );

// Loader flags

enum mmflag_t
{
	// Load as a resource dll
	MMFL_RESOURCEDLL = (1<<0),
	// By default the mapper will strip some info from the mapped module, this flag keeps everything intact
	MMFL_KEEP_INTACT = (1<<1),
	// Strips most of the PE headers except the absolutely necessary parts to run the dll, might interfere with some dlls
	MMFL_STRIP_PEHEADERS = (1<<2),
	// Do not change the page protections
	MMFL_SKIP_PROTECT = (1<<3),
};

// Manual Mapper

class CManualMap
{
public:
	void* MapDllFile( filesystem::file* bin, unsigned int flags );
	static NOINLINE bool UnmapDll( void* module, EntryPointFn pfn );
	
	friend bool Init( void* hmod );
	bool InitDllMain( void* hmod );

protected:
	void* Allocate( void* desired, unsigned long size, unsigned int flags );
	void Free( void* hmod );

	bool Validate( const void* image );
	void LoadFromFile( filesystem::file* bin, unsigned int hdrsize );
	unsigned int GrabDllName();

	bool MapDllPart2( unsigned int flags );
	bool FixRelocs( uintptr_t based );
	//bool FixRelocsEx( const void* image, unsigned int key, const pelite::ImageNtHeader* nt );
	bool LookupImports();
	bool LookupImports( const pelite::ImageImportDescriptor* desc );
	bool LookupXImports( const pelite::ImageXImportHeader* desc );
	bool ImportsFor( const pelite::ImageImportDescriptor* desc, void* hmod );
	bool XImportsFor( const pelite::ImageXImportModule* desc, unsigned int magic, void* hmod );
	void ApplyProtect();
	bool InitModule();
	
	void Finalize( unsigned int flags );
	void ClearDataDir( unsigned int dir );

	template< typename T >
	inline T MakePtr( unsigned long offset ) const
	{
		return MakePtr<T>( module, offset );
	}
	template< typename T >
	static inline T MakePtr( void* base, unsigned long offset )
	{
		return reinterpret_cast<T>( reinterpret_cast<uintptr_t>(base) + offset );
	}

private:
	void* module;
	pelite::ImageNtHeaders* nthdr;
	pelite::ImageSectionHeader* shdr;
	unsigned int myname;
};
