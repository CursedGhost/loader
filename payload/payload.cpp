#include "stdafx.h"

#include <meta/scrypt.h>
#include <hash/hash.h>
#include "payload.h"
#include "manualmap.h"
#include "log.h"

class CMemoryFile : public filesystem::file
{
public:
	CMemoryFile( const void* bin, unsigned long size, unsigned int key )
		: data((const unsigned char*)bin), size(size), pos(0), key(key) {
	}
	~CMemoryFile() { data = nullptr; size = 0; pos = 0; key = 0; }
	void operator delete( void* p ) { }

	// Inherited from filesystem::file
	virtual bool open( filesystem::hpath file, int mode ) { return false; }
	virtual void close() { }
	virtual int status() const { return 0; }
	virtual bool info( info_t& fi ) const { return false; }
	virtual size_t write( const void* src, size_t size ) { return 0; }
	virtual size_t vprintf( const char* fmt, va_list va = nullptr ) { return 0; }
	virtual void flush() { }

	virtual bool seek( pos_t offset, int origin ) {
		unsigned long off = static_cast<unsigned long>(offset);
		// Compute final offset
		unsigned long final = pos;
		if ( origin==BEGIN ) final = off;
		else if ( origin==CURRENT ) final = pos + off;
		else if ( origin==END ) final = size - off;
		else return false;
		// Check validity
		if ( final>size )
			return false;
		// Move pointer
		pos = final;
		return true;
	}
	virtual pos_t tell() const {
		return pos;
	}
	virtual size_t read( void* buf, size_t num, unsigned int term = -1 ) const {
		// Do not overflow
		if ( pos+num>size )
			num = size - pos;

		if ( key )
		{
			// Can only decrypt aligned
			if ( (pos&3)!=0 )
				return 0;
			// Obfuscated contents
			meta::SimpleDecrypt( data, pos, key, buf, num );
		}
		else
		{
			// Just copy bytes
			__movsb( (unsigned char*)buf, data+pos, num );
		}
		
		pos += num;
		return num;
	}
	
private:
	const unsigned char* data;
	unsigned long size;
	mutable unsigned long pos;
	unsigned int key;
};


void* PLAPI PlMapDll( const void* bin, unsigned long size, unsigned int flags, unsigned int key )
{
	CMemoryFile file( bin, size, key );
	return PlMapDllFile( &file, flags );
}
void* PLAPI PlMapDllFile( filesystem::file* bin, unsigned int flags )
{
	CManualMap mmap;
	void* hmod = mmap.MapDllFile( bin, flags );
	assert( hmod || ErrorGet() );
	return hmod;
}
bool PLAPI PlUnmapDll( void* hmod )
{
	return false;
}



class CPayloadManager
{
public:
	void Init( void* hmod, EntryPointFn pfn )
	{
		count = 1;
		module_t& self = modules[0];
		self.name = HASH("payload.dll");
		self.address = hmod;
		self.pfn = pfn;
		self.refc = 1;
	}

	module_t* Lookup( unsigned int name )
	{
		for ( auto it = modules, end = it+count; it!=end; ++it )
		{
			if ( it->name==name )
				return it;
		}
		return nullptr;
	}

	module_t& Push( void* hmod, EntryPointFn pfn, unsigned int name )
	{
		module_t& it = modules[count++];
		it.name = name;
		it.address = hmod;
		it.pfn = pfn;
		it.refc = 1;
		return it;
	}

private:
	unsigned int count;
	module_t modules[32];

public:
	static CPayloadManager g;
};
CPayloadManager CPayloadManager::g;


void PlInit( void* self, EntryPointFn pfn )
{
	CPayloadManager::g.Init( self, pfn );
}

module_t* PlPushDll( void* hmod, EntryPointFn pfn, unsigned int name )
{
	return &CPayloadManager::g.Push( hmod, pfn, name );
}

module_t* PlLookupDll( unsigned int name )
{
	return CPayloadManager::g.Lookup( name );
}
