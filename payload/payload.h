#pragma once

//----------------------------------------------------------------
// Main interface for the payload
//----------------------------------------------------------------

#include <filesystem/file.h>
#include "export.h"

typedef int (__stdcall* EntryPointFn)( void*, unsigned int, void* );

PLEXPORT void* PLAPI PlMapDll( const void* bin, unsigned long size, unsigned int flags = 0, unsigned int key = 0 );
PLEXPORT void* PLAPI PlMapDllFile( filesystem::file* bin, unsigned int flags = 0 );
PLEXPORT bool PLAPI PlUnmapDll( void* hmod );

// FIXME! Move me away from here!
PLEXPORT void* PLAPI PlLoadDllA( const char* dllname );
PLEXPORT void* PLAPI PlLoadDllW( const wchar_t* dllname );

struct module_t
{
	unsigned int name;
	void* address;
	EntryPointFn pfn;
	int refc;
};

void PlInit( void* self, EntryPointFn pfn );
module_t* PlPushDll( void* hmod, EntryPointFn pfn, unsigned int name );
module_t* PlLookupDll( unsigned int name );
