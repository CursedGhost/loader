#include "stdafx.h"

#include <meta/strcrypt.h>
#include "pipe.h"
#include "imports.h"
#include "log.h"
#include "hybrid.h"
#include "payload.h"
#include "utility.h"

unsigned long rand( unsigned long& seed )
{
	seed = ( seed * 22695477 ) + 1;
	return _byteswap_ulong( seed );
}

class CCommandPipe
{
public:
	bool Attach( unsigned long seed );
	void Detach();

	bool Write( const void* data, unsigned long size );
	template< typename T >
	inline bool Write( const T& t ){
		return Write( &t, sizeof(T) );
	}

	bool Read( void* buf, unsigned long size );
	template< typename T >
	inline bool Read( T& t ) {
		return Read( &t, sizeof(T) );
	}

	void Thread();
	
	void GetError();
	void SetError();
	void MapDll();
	void Call();

private:
	HANDLE hPipe;
};


long PLAPI PipeThread( unsigned long id )
{
	CCommandPipe pipe;
	if ( pipe.Attach( id ) )
	{
		pipe.Thread();
		return 0;
	}
	else
	{
//75AA786A  mov         eax,dword ptr fs:[00000018h]  
//75AA7870  mov         eax,dword ptr [eax+34h]  
//75AA7873  ret  
		// Return GetLastError
		long* a = (long*) __readfsdword( 0x18 );
		return a[0xD];
	}
}


bool CCommandPipe::Attach( unsigned long seed )
{
	// Generate a unique pipe name
	char name[32];
	STRASSIGN( name, "\\\\.\\pipe\\" );
	long len = 8 + ( rand( seed ) & 7 );
	for ( long i = 0; i<len; ++i ) {
		name[9+i] = 0x40 + ( rand( seed ) % 0x40 );
	}
	name[9+len] = 0;

	// Try to open it
	hPipe = Imports.CreateFileA( name, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL );
	if ( hPipe!=INVALID_HANDLE_VALUE )
	{
		return true;
	}
	return false;
}
void CCommandPipe::Detach()
{
	Imports.CloseHandle( hPipe );
}
bool CCommandPipe::Write( const void* data, unsigned long size )
{
	DWORD bytesWritten;
	return Imports.WriteFile( hPipe, data, size, &bytesWritten, NULL ) && bytesWritten==size;
}
bool CCommandPipe::Read( void* buf, unsigned long size )
{
	DWORD bytesRead;
	return Imports.ReadFile( hPipe, buf, size, &bytesRead, NULL ) && bytesRead==size;
}

void CCommandPipe::Thread()
{
	// This thread will keep reading commands from the pipe

	long command;
	while ( Read( command ) )
	{
		if ( command==PLCMD_EXIT )
		{
			Detach();
			break;
		}
		else if ( command==PLCMD_GETERROR )
		{
			GetError();
		}
		else if ( command==PLCMD_SETERROR )
		{
			SetError();
		}
		else if ( command==PLCMD_MAPDLL )
		{
			MapDll();
		}
		else if ( command==PLCMD_CALL )
		{
			Call();
		}
	}
}

NOINLINE void CCommandPipe::GetError()
{
	Write( &Error, 256 );
}
NOINLINE void CCommandPipe::SetError()
{
	char buf[252];
	Read( buf );
	ErrorSet( 13, STRDEF("%s"), buf );
}
NOINLINE void CCommandPipe::MapDll()
{
	// Read control data
	struct T
	{
		unsigned long size;
		unsigned long flags;
		unsigned long key;
	} t;
	Read( t );
	// Allocate memory for image
	unsigned char* data = rweAlloc( t.size );
	// Receive image in chunks of 512 bytes
	for ( unsigned long o = 0; o<t.size; o += 512 )
	{
		Read( data+o, 512 );
	}
	// Dispatch mapping
	void* hmod = PlMapDll( data, t.size, t.flags, t.key );
	// Cleanup
	rweFree( data );
	// Reply with module handle
	Write( hmod );
}
NOINLINE void CCommandPipe::Call()
{
	// Read function to call
	struct T
	{
		void* hmod;
		unsigned int func;
		unsigned int arg_len;
	} t;
	Read( t );
	// Read (optional) arg
	unsigned char* data = nullptr;
	if ( t.arg_len )
	{
		data = rweAlloc( t.arg_len );
		Read( data, t.arg_len );
	}
	// Lookup function name
	long s = -1;
	meta::declpfn<long (__stdcall*)( unsigned char* )> pfn( PlSymAddr( t.hmod, t.func ) );
	if ( pfn )
	{
		// Call it
		s = pfn( data );
	}
	// Free temp
	rweFree( data );
	// Write response back (-1 if export not found)
	Write( s );
}

