#pragma once

//----------------------------------------------------------------
// Pipe communication
//----------------------------------------------------------------
// Interact with the payload from another process through a pipe.

#include "export.h"

long PipeConnectId;

PLEXPORT long PLAPI PipeThread( long id );

// KEEP IN SYNC WITH plsdk/manager.h !
enum plcmd_t
{
	PLCMD_EXIT = 0,
	PLCMD_ACK,
	PLCMD_GETERROR,
	PLCMD_SETERROR,
	PLCMD_MAPDLL,
	PLCMD_CALL,
};

