#pragma once

// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#include "targetver.h"

// No Windows.h! FIXME! VS2013 complains...
#include <Windows.h>
#include <intrin.h>
#include <excpt.h>
typedef _CONTEXT* PCONTEXT;
#include <windef.h>
#include <winternl.h>

#include <cstdarg>

#define INVALID_HANDLE_VALUE ((HANDLE)(LONG_PTR)-1)
#define CURRENT_PROCESS_HANDLE ((HANDLE)(LONG_PTR)-1)
#define INVALID_FILE_SIZE ((DWORD)0xFFFFFFFF)
#define INVALID_SET_FILE_POINTER ((DWORD)-1)
#define INVALID_FILE_ATTRIBUTES ((DWORD)-1)

#define FILE_BEGIN           0
#define FILE_CURRENT         1
#define FILE_END             2

#define CREATE_NEW          1
#define CREATE_ALWAYS       2
#define OPEN_EXISTING       3
#define OPEN_ALWAYS         4
#define TRUNCATE_EXISTING   5

#define PROCESS_DEP_ENABLE                          0x00000001
#define PROCESS_DEP_DISABLE_ATL_THUNK_EMULATION     0x00000002
