#include "stdafx.h"

#include "syscall.h"

struct fsc_t
{
public:
	struct entry_t
	{
		unsigned short xp, vista, win7, win8, wow64, pad5, pad6, pad7;
	};
	enum
	{
		mask1 = 0x180,
		mask2 = 0x1F0,
	};

	bool Init();
	unsigned GetId( unsigned idx ) const;

public:
	typedef long (__fastcall* fn)( unsigned, va_list );
	fn fscimpl;

	unsigned ver;
	static const entry_t table[];

	static fsc_t g;
};

fsc_t fsc_t::g;

// The syscall table
// Supports XP SP0/1/2/3 Vista SP1/2 Win7 SP0/1
// BIG FAT WARNING! All this is relied upon in testapp/payload for randomizing the key!
// IF YOU MAKE A CHANGE HERE, UPDATE testapp/payload ACCORDINGLY!!!

#define ENCODE( IDX, VAL ) ( (unsigned short)( (VAL+fsc_t::mask2)^(fsc_t::mask1+IDX) ) )
#define DECODE( IDX, VAL ) ( (VAL^(fsc_t::mask1+IDX))-fsc_t::mask2 )
#define SYSCALL( IDX, XP, VISTA, WIN7, WIN8, WOW64 ) \
	{ ENCODE(IDX,XP), ENCODE(IDX,VISTA), ENCODE(IDX,WIN7), ENCODE(IDX,WIN8), ENCODE(IDX,WOW64), 0, 0, 0 }

const fsc_t::entry_t fsc_t::table[] =
{
	//----------- XP -- VISTA -- WIN7 --- WIN8 -- WOW64 --
	SYSCALL(  0, 0x0011, 0x0012, 0x0013, 0x0192, 0x0015 ), // NtAllocateVirtualMemory
	SYSCALL(  1, 0x0053, 0x0093, 0x0083, 0x0116, 0x001b ), // NtFreeVirtualMemory
	SYSCALL(  2, 0x0089, 0x00d2, 0x00d7, 0x00c2, 0x004d ), // NtProtectVirtualMemory
	SYSCALL(  3, 0x00b2, 0x00fd, 0x010b, 0x008e, 0x0020 ), // NtQueryVirtualMemory
	SYSCALL(  4, 0x00ba, 0x0105, 0x0115, 0x0082, 0x003c ), // NtReadVirtualMemory
	SYSCALL(  5, 0x0115, 0x0166, 0x018f, 0x0001, 0x0037 ), // NtWriteVirtualMemory
	SYSCALL(  6, 0x009a, 0x00e4, 0x00ea, 0x00af, 0x0016 ), // NtQueryInformationProcess
	SYSCALL(  7, 0x009b, 0x00e5, 0x00ec, 0x00b1, 0x0022 ), // NtQueryInformationThread
	SYSCALL(  8, 0x0101, 0x014e, 0x0172, 0x0023, 0x0029 ), // NtTerminateProcess
	{ 0, 0, 0, 0, 0, 0, 0, 0 },
};

unsigned int fsc_t::GetId( unsigned int idx ) const
{
	const entry_t& e = table[idx];
	unsigned int val = ((const unsigned short*)&e)[ver];
	// Only need to mask if it'll go outside that range in the first place.
	unsigned int id = DECODE( idx, val );
	return id;
}

#undef SYSCALL
#undef ENCODE
#undef DECODE




extern "C" long __fastcall _fsc_int( unsigned int id, va_list args );
extern "C" long __fastcall _fsc_sysenter( unsigned int id, va_list args );
extern "C" long __fastcall _fsc_wow64( unsigned int id, va_list args );

extern "C" fsc_t::fn __fastcall _fsc_select();

bool SystemInit()
{
	return fsc_t::g.Init();
}
long SystemCall( sysfn_t idx, ... )
{
	unsigned int id = fsc_t::g.GetId( idx );
	va_list va;
	va_start( va, idx );
	return fsc_t::g.fscimpl( id, va );
}




DECLSPEC_NOINLINE bool fsc_t::Init()
{
	// Get OS version
	unsigned int* peb = (unsigned int*) __readfsdword( 0x30 );
	unsigned int winMajor = peb[41]; // +0xA4
	unsigned int winMinor = peb[42]; // +0xA8

	// 5.1 -> (5-5)*2+(1-1) = 0
	// 6.0 -> (6-5)*2+(0-1) = 1
	// 6.1 -> (6-5)*2+(1-1) = 2
	// 6.2 -> (6-5)*2+(2-1) = 3
	ver = ( winMajor - 5 ) * 2 + ( winMinor - 1 );
	if ( ver>3 ) return false;

	// Init the callback
	fscimpl = _fsc_select();
	return true;
}

