#pragma once

//----------------------------------------------------------------
// Direct system calls
//----------------------------------------------------------------
// System call indices:
//  x86 http://j00ru.vexillium.org/ntapi/
//  x64 http://j00ru.vexillium.org/ntapi_64/

#include "export.h"

enum sysfn_t {
	kNtAllocateVirtualMemory,
	kNtFreeVirtualMemory,
	kNtProtectVirtualMemory,
	kNtQueryVirtualMemory,
	kNtReadVirtualMemory,
	kNtWriteVirtualMemory,

	kNtQueryInformationProcess,
	kNtQueryInformationThread,
	kNtTerminateProcess,

	kNtCreateFile,
	kNtReadFile,
	kNtWriteFile,
};

bool SystemInit();
PLEXPORT long SystemCall( sysfn_t idx, ... );
