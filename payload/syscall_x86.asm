TITLE syscall_x86

.MODEL FLAT
.CODE

@_fsc_int@8 PROC NEAR
	mov eax, ecx
	int 2Eh
	ret
@_fsc_int@8 ENDP

@_fsc_sysenter@8 PROC NEAR
	mov eax, ecx
	mov edx, esp
	sysenter
@_fsc_sysenter@8 ENDP

ASSUME fs:NOTHING
@_fsc_wow64@8 PROC NEAR
	mov eax, ecx
	xor ecx, ecx
	call dword ptr fs:[0C0h]
	add esp, 4
	ret
@_fsc_wow64@8 ENDP

@_fsc_select@0 PROC NEAR
	mov ecx, dword ptr fs:[0C0h]
	mov eax, @_fsc_wow64@8
	test ecx, ecx
	jnz finish
IFDEF _DEBUG
	mov eax, @_fsc_int@8
ELSE
	push ebx
	mov eax, 1
	cpuid
	mov eax, @_fsc_sysenter@8
	pop ebx
	test edx, 0800h
	jnz finish
	mov eax, @_fsc_int@8
ENDIF
finish LABEL NEAR
	ret
@_fsc_select@0 ENDP

END
