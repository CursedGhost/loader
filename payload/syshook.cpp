#include "stdafx.h"

#include "syshook.h"
#include "hybrid.h"
#include "utility.h"
#include <pelite/image.h>

class CSystemHook
{
public:

friend bool PLAPI SystemHook( SystemHookFn pfn );

bool Init( SystemHookFn pfn )
{
	if ( pfn && !pfnCallback )
	{
		return Construct( pfn );
	}
	else if ( !pfn && pfnCallback )
	{
		Destruct();
		return true;
	}
	return false;
}
bool Construct( SystemHookFn pfn )
{
	// Grab NTDLL While we're at it
	hmNTDLL = (HMODULE) PlModuleHandle( HASH("ntdll.dll") );

	// Initialize some bytes
	pfnCallback = pfn;
	pfnGate = (volatile long long*) __readfsdword( 0xC0 );
	fnOrigBytes = *pfnGate;

	// Generate a thunk to execute
	pfnHookThunk = GenerateThunk();

	// Change protect to allow patching
	DWORD oldP;
	void* addr = (void*)pfnGate;
	DWORD bytes = 8;
	NTSTATUS s = SystemCall( kNtProtectVirtualMemory, CURRENT_PROCESS_HANDLE, &addr, &bytes, PAGE_EXECUTE_READWRITE, &oldP );
	if ( NT_SUCCESS(s) ) {
		// Patch with our hook
		auto jmp = GenerateJmp( (uintptr_t)pfnGate, (uintptr_t)pfnHookThunk );
		auto gate = pfnGate;
		_InterlockedCompareExchange64( gate, jmp, fnOrigBytes );

		// Restore protections
		s = SystemCall( kNtProtectVirtualMemory, CURRENT_PROCESS_HANDLE, &addr, &bytes, oldP, &oldP );
		return true;
	}
	return false;
}
void Destruct()
{
	// Unhook and WAIT so we don't execute mid hook!
	//_InterlockedCompareExchange64( pfnGate, *pfnGate, fnOrigBytes );
	// Free the hook thunk
	rweFree( pfnHookThunk );
	// Clear the callback ptr so it can be called later
	pfnCallback = nullptr;
}
unsigned char* GenerateThunk()
{
	// Allocate memory for the trampoline
	unsigned char* code = rweAlloc( 32 );
	// pushad, push esp, call HookThunk, popad
	(long long&)code[0] = ((unsigned long long)((unsigned int)&HookThunk - ((unsigned int)code+2) - 5)<<24)|0x6100000000E85460LL;
	// Original bytes should just be a simple far jmp
	(long long&)code[8] = *pfnGate;
	return code;
}
unsigned long long GenerateJmp( uintptr_t from, uintptr_t to )
{
	// dest = src + 5 + off  ->  off = dest - src - 5
	return ((unsigned long long)(to - from - 5)<<8)|0xE9;
}

sysfn_t Translate( unsigned eax )
{
	// Translate the raw index into syscall enum
	return static_cast<sysfn_t>(eax);
}


struct Context
{
	// Result of pushad
	unsigned int edi, esi, ebp, esp, ebx, edx, ecx, eax;
};
static void __stdcall HookThunk( Context& ctx )
{
	SystemData_t data;

	data.post = false;
	data.nts = 0;
	data.skip = false;

	// Store the raw data
	data.eax = ctx.eax;
	data.ecx = ctx.ecx;
	data.args = (uintptr_t*) ctx.edx;
	data.num = (ctx.edx - ctx.esp) / 4;
	
	// Figure out which ntdll func was called
	g.TrackNtCalls( data, ctx );

	// Normalize the syscall index
	data.fn = static_cast<sysfn_t>(-1);
	
	// Invoke callback
	g.pfnCallback( data );
}

bool TrackNtCalls( SystemData_t& data, const Context& ctx )
{
	data.ntapi = false;
	data.ntstr = nullptr;

	if ( hmNTDLL )
	{
		// Get NTDLL size
		unsigned char* ntbase = (unsigned char*)hmNTDLL;
		auto nthdr = (pelite::ImageNtHeaders*)( ntbase + ((pelite::ImageDosHeader*)ntbase)->e_lfanew );
		auto ntsize = nthdr->OptionalHeader.SizeOfImage;
		// Get and check return address
		unsigned char* ret = *(unsigned char**) ctx.esp;
		if ( ret>=ntbase && ret<(ret+ntsize) )
		{
			data.ntapi = true;

			// NTDLL Export Table
			auto& expdir = nthdr->OptionalHeader.DataDirectory[pelite::DataDir_Export];
			if ( expdir.VirtualAddress )
			{
				// Iterate over exports to find where return address matches
				// FIXME! Binary search?
				auto* exports = (pelite::ImageExportDirectory*)( ntbase + expdir.VirtualAddress );
				unsigned int expfn = ~0;
				unsigned int rva = ret - ntbase;
				auto* funcs = (unsigned int*)( ntbase + exports->AddressOfFunctions );
				for ( auto it = funcs, end = it + exports->NumberOfFunctions; it!=end; ++it )
				{
					// Look for nearest function call
					// IDEA: Disassemble the function to find its length? (might be too expensive...)
					unsigned int func = *it;//(unsigned char*)( ntbase + *it );
					if ( rva>=func && rva<(func+0x20) && func<expfn )
					{
						expfn = func;
					}
				}

				if ( expfn!=~0 )
				{
					// Try to find matching export symbol
					auto* names = (unsigned int*)( ntbase + exports->AddressOfNames );
					auto* nameords = (unsigned short*)( ntbase + exports->AddressOfNameOrdinals );
					const char* expname = nullptr;
					for ( unsigned int i = 0; i<exports->NumberOfNames; ++i )
					{
						// Check if matching function
						unsigned int index = static_cast<unsigned int>(nameords[i]) - exports->Base;
						if ( funcs[index]==expfn )
						{
							// Named export, favor 'Nt' over 'Zw'
							const char* fname = (const char*)( ntbase + names[i] );
							if ( !expname || !( fname[0]=='Z' && fname[1]=='w' ) )
							{
								expname = fname;
							}
						}
					}

					// Found exported name!
					if ( expname )
					{
						data.ntstr = expname;
						return true;
					}
				}
			}
		}
	}
	return false;
}


private:
	SystemHookFn pfnCallback;
	volatile long long* pfnGate;
	long long fnOrigBytes;
	unsigned char* pfnHookThunk;
	HMODULE hmNTDLL;
	static CSystemHook g;
};

CSystemHook CSystemHook::g;

bool PLAPI SystemHook( SystemHookFn pfn )
{
	return CSystemHook::g.Init( pfn );
}
