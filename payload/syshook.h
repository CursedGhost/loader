#pragma once

//----------------------------------------------------------------
// System call hooks
//----------------------------------------------------------------
// Invoke SystemHook( callback ) to initialize the hook
// Invoke SystemHook( NULL ) to destroy the hook
//
// FIXME! On 32bitOS sysenter takes arguments in a weird way (arguments are garbage)!

#include "syscall.h"

struct SystemData_t
{
	// Raw syscall values
	unsigned int eax;
	unsigned int ecx;
	// Normalized syscall index, -1 if unknown
	sysfn_t fn;
	// Number of arguments
	unsigned int num;
	// Pointer to first argument
	uintptr_t* args;
	// Return value (only valid in post)
	NTSTATUS nts;
	// In a pre-callback, when true, will not perform syscall and use nts as return value, post not invoked
	bool skip;
	// Timing, false = before syscall, true = post syscall
	bool post;
	// Call comes from NTDLL
	bool ntapi;
	// If exported from NTDLL, attempt to find which export (null if not found)
	const char* ntstr;
};
// Signature for a hook callback
typedef void (PLAPI* SystemHookFn)( SystemData_t& data );

// Initialize and Shutdown
PLEXPORT bool PLAPI SystemHook( SystemHookFn pfn );
