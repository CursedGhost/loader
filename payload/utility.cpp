#include "stdafx.h"

#include <pelite/image.h>
#include <toolkit/tools.h>
#include "utility.h"
#include "log.h"
#include "payload.h"


//typedef struct _PEB_LDR_DATA  
//{ 
//  ULONG                   Length; 
//  BOOLEAN                 Initialized; 
//  PVOID                   SsHandle; 
//  LIST_ENTRY              InLoadOrderModuleList; 
//  LIST_ENTRY              InMemoryOrderModuleList; 
//  LIST_ENTRY              InInitializationOrderModuleList; 
//} PEB_LDR_DATA, *PPEB_LDR_DATA; 

struct LDR_MODULE
{
	struct LIST_ENTRY
	{
		LIST_ENTRY* Flink;
		LIST_ENTRY* Blink;
	};
	LIST_ENTRY InLoadOrderModuleList;
	LIST_ENTRY Unk01;
	void* Module;
	void* EntryPoint;
	unsigned int SizeOfImage;
	UNICODE_STRING FullDllPath;
	UNICODE_STRING DllName;
	unsigned int Unk05;
	unsigned int RefCount;
	unsigned int Unk07[8];
};
typedef LDR_MODULE* PLDR_MODULE;

void IncrementRefCount( void* module )
{
	PPEB peb = (PPEB) __readfsdword( 0x30 );
	PLIST_ENTRY list = &peb->Ldr->InMemoryOrderModuleList;
	
	// Search through the process' module list
	for ( PLIST_ENTRY it = list->Flink; it!=list; it = it->Flink )
	{
		PLDR_MODULE ldr = (PLDR_MODULE) it;

		if ( module>=ldr->Module && module<((char*)ldr->Module+ldr->SizeOfImage) )
		{
			++ldr->RefCount;
		}
	}
}

PLEXPORT void* PLAPI PlModuleHandle( unsigned int name )
{
	if ( name )
	{
		// Lookup in the cache first! Reason is simple, if a dll is not found it is manual mapped and added to this cache.
		// Should the dll be LoadLibrary'd later (by the host for example) we rather want to return our own manual mapped version instead.
		if ( module_t* hmod = PlLookupDll( name ) )
			return hmod->address;

		PPEB peb = (PPEB) __readfsdword( 0x30 );
		PLIST_ENTRY list = &peb->Ldr->InMemoryOrderModuleList;
	
		// Search through the process' module list
		for ( PLIST_ENTRY it = list->Flink; it!=list; it = it->Flink )
		{
			PLDR_MODULE ldr = (PLDR_MODULE) it;

			if ( WrapDllName( ldr->DllName.Buffer )==name )
				return ldr->Module;
		}
		// Not found
		return nullptr;

	}
	else
	{
		// Get the module handle of the main module if 0
		// Ripped from RE'd kernel32.dll
		uintptr_t p1 = __readfsdword( 0x18 );
		uintptr_t p2 = *(uintptr_t*)(p1+0x30);
		uintptr_t p3 = *(uintptr_t*)(p2+0x8);
		return (void*)p3;
	}
}
PLEXPORT void* PLAPI PlModuleHandleA( const char* name )
{
	return PlModuleHandle( name?WrapDllName( name ):(unsigned int)name );
}
PLEXPORT void* PLAPI PlModuleHandleW( const wchar_t* name )
{
	return PlModuleHandle( name?WrapDllName( name ):(unsigned int)name );
}



void* PlSymCheckHook( void* hmod, const unsigned int& psym )
{
	using namespace pelite;
	using toolkit::make_ptr;
	
	unsigned int sym = psym;

	// This is supposed to detect & route around EAT hooks...
	// Problems:
	// * Imports are loaded THROUGH here, possibly before the required file imports are fetched! Direct syscalls?
	// * Relies heavily on PE headers, not available in manually mapped modules!
	// * Define those RVA/VA/FileOffset converter funcs... Add them to pelite/standalone.h!
	// * Windows might hook itself, this can potentially break the app!
	// * File I/O every time a hook is detected (not efficient when used in batches)!
	//
	// A lot of work for something that'll barely be used anyway...
	//

	/*
	ImageNtHeader* nt = make_ptr<ImageNtHeader*>( hmod, ((ImageDosHeader*) hmod)->e_lfanew );
	unsigned int size = nt->OptionalHeader.SizeOfImage;
	if ( size && sym>size )
	{
		// EAT hook detected, read original bytes from disk!
		// Note, it's possible that the required APIs are not yet imported!
		// (example: CreateFileW has been hooked but we need that one to do our job)

		if ( IAT.CreateFileW && IAT.SetFilePointer && IAT.ReadFile && IAT.CloseHandle )
		{
			const wchar_t* file;
			HANDLE hFile;
			if ( PlModuleName( hmod, hmod, file ) &&
				( hFile = IAT.CreateFileW( file, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL ) )!=INVALID_HANDLE_VALUE )
			{
				unsigned int off = RvaToFileOffset( hmod, ((unsigned int)&sym) - ((unsigned int)hmod) );
				IAT.SetFilePointer( hFile, off, 0, FILE_BEGIN );

				DWORD read;
				IAT.ReadFile( hFile, &sym, sizeof(sym), &read, NULL );

				IAT.CloseHandle( hFile );
			}
		}
	}
	*/

	void* addr = NULL;
	if ( sym )
		addr = make_ptr( hmod, sym );
	return addr;
}

void* PlRedirSymAddr( unsigned int name )
{
	// API Redirectors...

	switch ( name )
	{
	case 0xaadfab0b: //if ( name==HASH("GetProcAddress") )
		return &PlSymAddrA;
	case 0x4ab16d82: //if ( name==HASH("GetModuleHandleA") )
		return &PlModuleHandleA;
	case 0x4ab16d94: //if ( name==HASH("GetModuleHandleW") )
		return &PlModuleHandleW;
	//case 0x809469ce: //if ( name==HASH("FreeLibrary") )
	//	return &WinFreeLibrary;
	//case 0x01ed9add: //if ( name==HASH("LoadLibraryA") )
	//	return &WinLoadLibraryA;
	//case 0x01ed9acb: //if ( name==HASH("LoadLibraryW") )
	//	return &WinLoadLibraryW;
	default:
		return NULL;
	}
}

PLEXPORT void* PLAPI PlSymAddr( void* hmod, unsigned int name )
{
	using namespace pelite;
	using toolkit::make_ptr;

	ErrorClear();

	if ( void* addr = PlRedirSymAddr( name ) )
	{
		return addr;
	}

	// Locate the export directory

	ImageNtHeaders* nt = make_ptr<ImageNtHeaders*>( hmod, ((ImageDosHeader*) hmod)->e_lfanew );
	ImageDataDirectory& dir = nt->OptionalHeader.DataDirectory[ DataDir_Export ];

	if ( !dir.VirtualAddress )
	{
		LOG_ERROR( 0x40, "PlSymAddr() No export directory!", "%x", name );
		return false;
	}

	ImageExportDirectory* exp = make_ptr<ImageExportDirectory*>( hmod, dir.VirtualAddress );

	// Traverse it to find the desired export

	unsigned int* names = make_ptr<unsigned int*>( hmod, exp->AddressOfNames );

	for ( unsigned int it = 0; it<exp->NumberOfNames; ++it )
	{
		// Get the name for this export

		const char* str = make_ptr<const char*>( hmod, names[it] );

		if ( hash::hash( str )==name )
		{
			// Index for this export

			int idx = make_ptr<word*>( hmod, exp->AddressOfNameOrdinals )[ it ];

			// Map it to actual function

			unsigned int& sym = make_ptr<unsigned int*>( hmod, exp->AddressOfFunctions )[ idx ];
			if ( sym>=dir.VirtualAddress && sym<(dir.VirtualAddress+dir.Size) )
			{
				// Forwarded to another dll
				return SymFwdAddr( make_ptr<const char*>( hmod, sym ) );
			}

			return PlSymCheckHook( hmod, sym );
		}
	}

	LOG_ERROR( 0x41, "PlSymAddr() Failed to find %x for %s!", "%x!%s", name, make_ptr<const char*>( hmod, exp->Name ) );
	return false;
}
PLEXPORT void* PLAPI PlSymAddrA( void* hmod, const char* name )
{
	using namespace pelite;
	using toolkit::make_ptr;

	ErrorClear();

	if ( void* addr = PlRedirSymAddr( hash::hash( name ) ) )
	{
		return addr;
	}

	// Locate the export directory

	ImageNtHeaders* nt = make_ptr<ImageNtHeaders*>( hmod, ((ImageDosHeader*) hmod)->e_lfanew );
	ImageDataDirectory& dir = nt->OptionalHeader.DataDirectory[ DataDir_Export ];

	if ( !dir.VirtualAddress )
	{
		LOG_ERROR( 0x40, "PlSymAddrA() No export directory!", "%s", name );
		return false;
	}

	ImageExportDirectory* exp = make_ptr<ImageExportDirectory*>( hmod, dir.VirtualAddress );

	// Traverse it to find the desired export

	unsigned int* names = make_ptr<unsigned int*>( hmod, exp->AddressOfNames );

	for ( unsigned int it = 0; it<exp->NumberOfNames; ++it )
	{
		// Get the name for this export

		const char* str = make_ptr<const char*>( hmod, names[it] );

		if ( !strcmp( str, name ) )
		{
			// Ordinal for this export

			int ord = make_ptr<word*>( hmod, exp->AddressOfNameOrdinals )[ it ];

			// Map it to actual function

			unsigned int& sym = make_ptr<unsigned int*>( hmod, exp->AddressOfFunctions )[ ord ];
			if ( sym>=dir.VirtualAddress && sym<(dir.VirtualAddress+dir.Size) )
			{
				// Forwarded to another dll
				return SymFwdAddr( make_ptr<const char*>( hmod, sym ) );
			}

			return PlSymCheckHook( hmod, sym );
		}
	}
	
	LOG_ERROR( 0x41, "PlSymAddrA() Failed to find \"%s\" for %s!", "%s!%s", name, make_ptr<const char*>( hmod, exp->Name ) );
	return false;
}
PLEXPORT void* PLAPI PlSymOrd( void* hmod, unsigned int ord )
{
	using namespace pelite;
	using toolkit::make_ptr;

	ErrorClear();

	ImageNtHeaders* nt = make_ptr<ImageNtHeaders*>( hmod, ((ImageDosHeader*) hmod)->e_lfanew );
	ImageDataDirectory& dir = nt->OptionalHeader.DataDirectory[ DataDir_Export ];

	void* func = NULL;

	if ( dir.VirtualAddress )
	{
		ImageExportDirectory* exp = make_ptr<ImageExportDirectory*>( hmod, dir.VirtualAddress );

		unsigned int index = ord - exp->Base;
		if ( index<exp->NumberOfFunctions )
		{
			const unsigned int* funcs = make_ptr<const unsigned int*>( hmod, exp->AddressOfFunctions );
			return PlSymCheckHook( hmod, funcs[ index ] );
		}
		else
		{
			LOG_ERROR( 0x42, "PlSymOrd() Failed to find %d for %s!", "%d!%s", ord, make_ptr<const char*>( hmod, exp->Name ) );
		}
	}
	else
	{
		LOG_ERROR( 0x40, "PlSymOrd() No export directory!", "" );
	}

	return func;
}
void* SymFwdAddr( const char* it )
{
	// Give it a .dll extension

	char dll[8]; ((int*)dll)[0] = *(const int*)".dll"; ((int*)dll)[1] = 0;
	filesystem::path path( it );
	path.replace_ext( dll );

	union
	{
		void* hmod;
		void* fn;
	};

	// Try to load it
	// FIXME! How do I manage dlls that get loaded at this point?
	// This is why originally I only had PlModuleHandleA, however windows 8 likes to forward a lot of COM stuff...

	if ( ( hmod = PlModuleHandleA( path.buffer ) ) || ( hmod = PlLoadDllA( path.buffer ) ) )
	{
		// Find actual name
		while ( *it++!='.' );
		fn = PlSymAddrA( hmod, it );
	}
	// This error should be set by PlLoadDllA!
	else
	{
		assert( ErrorGet() );
	}
	return fn;
}
