#pragma once

//----------------------------------------------------------------
// Utility stuff
//----------------------------------------------------------------
//
// Replacements for some Win APIs
// Tested on XP SP3, Vista SP2 and Win7
//
// Names are hashed, works with both char and wchar_t as long as you keep things ascii
//

#include <hash/hash.h>
#include "export.h"

template< typename T >
NOINLINE unsigned int PLAPI WrapDllName( const T* dllname )
{
	// Scan to the file name

	for ( const T* it = dllname; *it; ++it )
	{
		if ( *it=='\\' || *it=='/' )
			dllname = it+1;
	}

	// Lower case while generating hash

	typedef hash::hash_t hash_t;
	hash_t::type h = hash_t::init;
	for ( const T* it = dllname; *it; ++it )
	{
		hash_t::type c = static_cast<types::make_unsigned<T>::type>(*it);
		if ( c>='A' && c<='Z' ) c = c-'A'+'a';
		h = hash_t::algo( h, c );
	}

	return h;
}

PLEXPORT void* PLAPI PlModuleHandle( unsigned int name );
PLEXPORT void* PLAPI PlModuleHandleA( const char* name );
PLEXPORT void* PLAPI PlModuleHandleW( const wchar_t* name );
PLEXPORT void* PLAPI PlSymAddr( void* module, unsigned int name );
PLEXPORT void* PLAPI PlSymAddrA( void* module, const char* name );
PLEXPORT void* PLAPI PlSymOrd( void* module, unsigned int ord );
void* SymFwdAddr( const char* name );

void IncrementRefCount( void* module );
