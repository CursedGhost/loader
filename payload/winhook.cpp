#include "stdafx.h"

#include "export.h"
#include "imports.h"
#include "utility.h"

// For use with SetWindowsHookEx
PLEXPORT LRESULT __stdcall InjectHookEx( int code, WPARAM w, LPARAM l )
{
	// We have been injected!
	// Increment our reference count so we don't get ejected.
	IncrementRefCount( &InjectHookEx );
	// We do not CallNextHookEx since we'll be gone soon anyway...
	return 0;
}