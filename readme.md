
The Loader Project
--------------------

Injector for Windows.

Project dummy; helper dll to test injector.

Project mmap; stand alone executable for injector. Injects in a variety of ways and can detect & dump manual mapped dlls.

Project payload; assists with manual mapping and comes with an API for the injected dlls. Comes with an SDK.
