
#include <Windows.h>
#include <TlHelp32.h>
#include <pelite/image.h>
#include <toolkit/tools.h>
#include <meta/scrypt.h>
#include <meta/strcrypt.h>
#include <filesystem/file.h>
#include "manager.h"

namespace plsdk
{

//------------------------------------------------
// Utilities
//------------------------------------------------

bool GenerateTempPath( filesystem::path& out, filesystem::hpath hint )
{
	char buf[8];

	// Optional hint, default to temp folder
	if ( !hint )
	{
		hint = STRASSIGN( buf, "*TEMP*\\" );
	}

	// Expand env vars for convenience, must expand to an absolute path!
	out = hint;
	if ( !out.expand('*') || !out.is_absolute() )
	{
		return false;
	}
	
	// Just assume if no extension a directory is intended...
	// Work around to make sure stuff like %TEMP% (which doesn't have a trailing slash) works
	bool dir;
	if ( dir = !*out.ext() )
	{
		out.make_dir();
	}

	// Filename template
	// WARNING! % in the path are fine, they should be escaped in the filename!!
	// FIXME! Do this check myself and reject malformed formatting strings?
	const char* fmt = (!dir)?
		hint->filename():
		STRASSIGN( buf, "%x.tmp" );

	// Generate the filename
	unsigned int x = meta::SimpleIV();
	char* filename = out.filename();
	::vsnprintf( filename, sizeof(out)-(filename-out.buffer), fmt, (va_list)&x );
	out.buffer[sizeof(out.buffer)-1] = '\0';

	return true;
}
// Helper to dump the payload to a random file on disk
bool WriteTempFile( filesystem::path& out, filesystem::file* file, const char* hint )
{
	// Generate a temp file path
	if ( GenerateTempPath( out, hint ) )
	{
		// Create the file
		HANDLE hFile = ::CreateFileA( out.buffer, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
		if ( hFile!=INVALID_HANDLE_VALUE )
		{
			// Dump it to file in chunks of 512 bytes
			char chunk[512];
			DWORD written;
			file->rewind();

			// Decrypt a chunk
			int read;
			while ( ( read = file->read( chunk, sizeof(chunk) ) )>0 )
			{
				// Write chunk to file
				::WriteFile( hFile, chunk, read, &written, NULL );
			}

			::CloseHandle( hFile );
			return true;
		}
	}
	return false;
}

//------------------------------------------------
// Payload exports
//------------------------------------------------
// Read the exports directly
void Exports::Load( void* hmod )
{
	using namespace pelite;
	using toolkit::make_ptr;

	ImageNtHeaders* nt = make_ptr<ImageNtHeaders*>( hmod, ((ImageDosHeader*)hmod)->e_lfanew );

	DllMain.pfn = (char*)hmod + nt->OptionalHeader.AddressOfEntryPoint;

	ImageExportDirectory* edir = make_ptr<ImageExportDirectory*>( hmod, nt->OptionalHeader.DataDirectory[DataDir_Export].VirtualAddress );
	dword* funcs = make_ptr<dword*>( hmod, edir->AddressOfFunctions );

	unsigned int num = sizeof(Exports)/sizeof(void*) - 1;
	for ( unsigned int i = 0; i<num; ++i )
	{
		((dword*)this)[i+1] = (dword)hmod + funcs[i];
	}
}
inline unsigned int RvaToFileOffset( pelite::ImageNtHeaders* nt, unsigned int rva )
{
	using namespace pelite;
	using toolkit::make_ptr;

	ImageSectionHeader* it = make_ptr<ImageSectionHeader*>( &nt->OptionalHeader, nt->FileHeader.SizeOfOptionalHeader );
	for ( ImageSectionHeader* end = it+nt->FileHeader.NumberOfSections; it<end; ++it )
	{
		if ( rva>it->VirtualAddress && rva<(it->VirtualAddress+it->VirtualSize) )
			return ( rva - it->VirtualAddress ) + it->PointerToRawData;
	}
	return 0;
}
void Exports::Load( filesystem::file* pl )
{
	using namespace pelite;
	using toolkit::make_ptr;

	// Read the PE headers
	char buf[0x400];
	pl->rewind();
	pl->read( buf, sizeof(buf) );
	ImageNtHeaders* nt = make_ptr<ImageNtHeaders*>( buf, ((ImageDosHeader*)buf)->e_lfanew );
	ImageSectionHeader* hdr = make_ptr<ImageSectionHeader*>( &nt->OptionalHeader, nt->FileHeader.SizeOfOptionalHeader );

	DllMain = (void*)nt->OptionalHeader.AddressOfEntryPoint;

	// Export directory
	ImageExportDirectory edir;
	pl->seek( RvaToFileOffset( nt, nt->OptionalHeader.DataDirectory[DataDir_Export].VirtualAddress ), pl->BEGIN );
	pl->read( &edir, sizeof(edir) );

	// Exported function list
	pl->seek( RvaToFileOffset( nt, edir.AddressOfFunctions ), pl->BEGIN );
	pl->read( ((void**)this)+1, sizeof(Exports)-sizeof(Exports::DllMain) );

	// Offset for RVA
	// This code path is only used by the Manager class which calls this function
	// Before it's actually loaded so we don't know where it'll end up.
	// It'll compute the final address at runtime in the stub.
	//for ( unsigned long* it = (unsigned long*)&MapDll, *end = it+16; it<end; ++it )
	//	*it += (unsigned long)hmod;
}


//------------------------------------------------
// class Process
//------------------------------------------------

Process::Process() : pid(0), tid(0), hProcess(NULL) {}
Process::~Process()
{
	Detach();
}
NOINLINE bool Process::Attach( DWORD pid, DWORD tid )
{
	assert( !isAttached() );
	this->pid = pid;
	this->tid = tid;
	DWORD rights = PROCESS_VM_OPERATION|PROCESS_VM_READ|PROCESS_VM_WRITE|PROCESS_CREATE_THREAD|PROCESS_QUERY_INFORMATION|PROCESS_SUSPEND_RESUME|SYNCHRONIZE;
	hProcess = ::OpenProcess( rights, FALSE, pid );
	return hProcess!=NULL;
}
NOINLINE bool Process::Attach()
{
	assert( !isAttached() );
	pid = ::GetCurrentProcessId();
	tid = ::GetCurrentThreadId();
	hProcess = INVALID_HANDLE_VALUE; //::GetCurrentProcess();
	return true;
}
NOINLINE void Process::Detach()
{
	if ( hProcess && hProcess!=INVALID_HANDLE_VALUE )
	{
		::CloseHandle( hProcess );
	}
	hProcess = NULL;
}
bool Process::Read( LPCVOID ptr, LPVOID store, SIZE_T bytes )
{
	SIZE_T numRead;
	return ::ReadProcessMemory( hProcess, ptr, store, bytes, &numRead )!=0 && bytes==numRead;
}
bool Process::Write( LPVOID dest, LPCVOID src, SIZE_T bytes )
{
	SIZE_T numWritten;
	return ::WriteProcessMemory( hProcess, dest, src, bytes, &numWritten )!=0 && bytes==numWritten;
}
LPVOID Process::Alloc( LPVOID ptr, SIZE_T bytes, DWORD type, DWORD prot )
{
	return ::VirtualAllocEx( hProcess, ptr, bytes, type, prot );
}
bool Process::Free( LPVOID ptr, SIZE_T bytes, DWORD type )
{
	return ::VirtualFreeEx( hProcess, ptr, bytes, type )!=FALSE;
}
bool Process::Protect( LPVOID ptr, SIZE_T bytes, DWORD prot, DWORD& old )
{
	return ::VirtualProtectEx( hProcess, ptr, bytes, prot, &old )!=FALSE;
}
HANDLE Process::CreateThread( LPTHREAD_START_ROUTINE pStartRoutine, LPVOID pParam, DWORD* tid )
{
	return ::CreateRemoteThread( hProcess, NULL, 0, pStartRoutine, pParam, 0, tid );
}
bool Process::Query( LPCVOID addr, MEMORY_BASIC_INFORMATION& info )
{
	return ::VirtualQueryEx( hProcess, addr, &info, sizeof(info) )>0;
}
bool Process::EnumProcess( const char* name, HANDLE& snap, PROCESSENTRY32& pe )
{
	pe.dwSize = sizeof(pe);

	// Create snapshot on first run
	if ( snap==INVALID_HANDLE_VALUE )
	{
		snap = ::CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
		if ( snap==INVALID_HANDLE_VALUE || !::Process32First( snap, &pe ) )
			goto cleanup;
	}
	// Else grab the next process
	else if ( !::Process32Next( snap, &pe ) )
	{
		goto cleanup;
	}

	// Iterate and return for every process found
	do
	{
		// FIXME! Case insensitive...
		if ( !name || !_strcmpi( name, pe.szExeFile ) )
		{
			return true;
		}
	}
	while ( ::Process32Next( snap, &pe ) );

cleanup:
	// Finished iterating
	::CloseHandle( snap );
	snap = INVALID_HANDLE_VALUE;

	return false;
}
bool Process::EnumModules( const char* name, DWORD pid, HANDLE& snap, MODULEENTRY32& me )
{
	me.dwSize = sizeof(me);

	// Create snapshot on first run
	if ( snap==INVALID_HANDLE_VALUE )
	{
		snap = ::CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, pid );
		if ( snap==INVALID_HANDLE_VALUE || !::Module32First( snap, &me ) )
		{
			DWORD err = ::GetLastError();
			goto cleanup;
		}
	}
	// Else grab next module
	else if ( !::Module32Next( snap, &me ) )
	{
		goto cleanup;
	}

	// Only care about module names, not the path
	if ( name )
	for ( const char* it = name; *it; ++it )
	{
		if ( *it=='/' || *it=='\\' )
			name = it+1;
	}

	// Iterate and return for every module found
	do
	{
		if ( !name || !_strcmpi( name, me.szModule ) )
		{
			return true;
		}
	}
	while ( ::Module32Next( snap, &me ) );

cleanup:
	// Finished iterating
	::CloseHandle( snap );
	snap = INVALID_HANDLE_VALUE;

	return false;
}



//------------------------------------------------
// class Manager
//------------------------------------------------
Manager::Manager( Process& proc, bool debug ) : proc(proc),
	debug(debug), hasdep(true),
	hPipe(INVALID_HANDLE_VALUE),
	//hThread(INVALID_HANDLE_VALUE),
	hModule(NULL)
{
}
Manager::~Manager()
{
	::CloseHandle( hPipe );
	hPipe = INVALID_HANDLE_VALUE;
}

//------------------------------------------------
// Interaction system
//------------------------------------------------
bool Manager::Q_Begin( filesystem::file* pl, const char* hint )
{
	filesystem::path path;
	return
		// Create the temporary file for payload
		Q_Payload( path, pl, hint ) &&
		// Inject the payload
		( hModule = InjectDll( path ) ) &&
		// Begin the remote command stuff
		Q_Launch();
}
void Manager::Q_End()
{
	// Send the exit command
	Q_Send( Q_PLCMD_EXIT );
	// Wait for it
	// FIXME! Do I really need to wait?
	//::WaitForSingleObject( hThread, INFINITE );
	// All done now
	::CloseHandle( hPipe );
	//::CloseHandle( hThread );
	//hThread = INVALID_HANDLE_VALUE;
	hPipe = INVALID_HANDLE_VALUE;
}
void Manager::Q_GetError( Error& err )
{
	if ( !( Q_Send( Q_PLCMD_GETERROR ) && Q_Recv(&err,sizeof(err)) ) )
	{
		// Rather than returning false, put something in the error :)
		err.code = ::GetLastError();
		STRASSIGN( err.buf, "GRfail!" );
	}
}
bool Manager::Q_SetError( const char* str )
{
	return Q_Send( Q_PLCMD_SETERROR ) && Q_Send( str, strlen(str)+1 );
}
void* Manager::Q_MapDll( filesystem::file* bin, unsigned flags, Error& err )
{
	// Get file info and send the map dll command
	filesystem::file::info_t info;
	if ( bin->info( info ) && Q_Send( Q_PLCMD_MAPDLL ) )
	{
		// Gather meta-data required for the mapper to work
		struct T
		{
			unsigned long size;
			unsigned long flags;
			unsigned long key;
		} t;
		t.size = static_cast<unsigned long>(info.size);
		t.flags = flags;
		t.key = meta::SimpleIV();
		// Send this meta data
		if ( Q_Send( &t, sizeof(t) ) )
		{
			// Send the file in chunks of 512 bytes
			unsigned char buf[512];
			filesystem::file::size_t bytes;
			bin->rewind();
			unsigned int off = 0;
			while ( ( bytes = bin->read( buf, sizeof(buf) ) )>0 )
			{
				meta::SimpleEncrypt( buf, off, t.key, sizeof(buf) );;
				off += bytes;
				if ( !Q_Send( buf, bytes ) )
					return nullptr;
			}
			// Wait for the response
			void* hmod;
			if ( Q_Recv( &hmod, sizeof(hmod) ) && hmod )
				return hmod;
		}
	}
	Q_GetError( err );
	return nullptr;
}
long Manager::Q_Call( void* hmod, unsigned int func, const void* data, unsigned int len, Error& err )
{
	if ( Q_Send( Q_PLCMD_CALL ) )
	{
		struct T
		{
			void* hmod;
			unsigned int func;
			unsigned int arg_len;
		} t;
		t.hmod = hmod;
		t.func = func;
		t.arg_len = len;
		if ( Q_Send( t ) && Q_Send( data, len ) )
		{
			long ret;
			if ( Q_Recv( &ret, sizeof(ret) ) )
				return ret;
		}
	}
	Q_GetError( err );
	return -2;
}



bool Manager::Q_Send( const void* data, unsigned size )
{
	DWORD bytes;
	return ::WriteFile( hPipe, data, size, &bytes, NULL )!=FALSE; //&& bytes==size
}
bool Manager::Q_Recv( void* buf, unsigned size )
{
	DWORD bytes;
	return ::ReadFile( hPipe, buf, size, &bytes, NULL )!=FALSE; //&& bytes==size;
}
bool Manager::Q_Payload( filesystem::path& path, filesystem::file* pl, const char* hint )
{
	if ( WriteTempFile( path, pl, hint ) )
	{
		exp.Load( pl );
		return true;
	}
	return false;
}
bool Manager::Q_Launch()
{
	// Need some randomness...
	unsigned long seed = static_cast<unsigned long>(__rdtsc());
	class rnd_t
	{
	public:
		rnd_t( unsigned long seed ) : seed( seed ) {
		}
		inline unsigned long operator() () {
			seed = ( seed * 22695477 ) + 1;
			return _byteswap_ulong( seed );
		}
	private:
		unsigned long seed;
	} rnd( seed );
	
	// Generate a unique pipe name
	//auto name = STRDEF("\\\\.\\pipe\\");
	char name[32];
	STRASSIGN( name, "\\\\.\\pipe\\" );
	long len = 8 + ( rnd() & 7 );
	for ( long i = 0; i<len; ++i ) {
		name[9+i] = 0x40 + ( rnd() % 0x40 );
	}
	name[9+len] = 0;

	// Create the named pipe
	// 512 byte buffers (large stuff is sent in chunks)
	hPipe = ::CreateNamedPipeA( name,
		PIPE_ACCESS_DUPLEX | FILE_FLAG_FIRST_PIPE_INSTANCE,
		PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
		PIPE_UNLIMITED_INSTANCES,
		512, 512, 5000, NULL );
	
	if ( hPipe!=INVALID_HANDLE_VALUE )
	{
		// Launch the client
		// For now I'm ditching the thread handle, I don't really need it anymore.
		if ( HANDLE hThread = proc.CreateThread(
			(LPTHREAD_START_ROUTINE)( (uintptr_t)hModule + (uintptr_t)exp.PipeThread.pfn ),
			(LPVOID)seed ) )
		{
			::CloseHandle( hThread );
			// Need to wait for client to accept the pipe
			// TODO! Time out of sorts...
			do Sleep( 1 );
			while ( !Q_Send( Q_PLCMD_ACK ) );
			return true;
		}
		::CloseHandle( hPipe );
		hPipe = INVALID_HANDLE_VALUE;
		//hThread = INVALID_HANDLE_VALUE;
	}

	return false;
}


// DLL Injection
NOINLINE void* Manager::InjectDll( filesystem::hpath file, void* temp_mem )
{
	// FIXME! This function has design problems!!!

	// Full path required because this won't be the same in the context of the target process!
	// Relative paths get current directory pasted at the start.
	// FIXME! What about system dlls? They'll get absoluted to current directory even though you may not want to...
	//        For now not a problem because that's not the use case for 99% of the calls to here but still...
	// IDEA!  Group this with ExpandEvironmentVariables and GetCurrentDirectory? (can be reused by GenerateTempPath)
	filesystem::path path;
	if ( file->is_absolute() )
	{
		path = file;
	}
	else
	{
		::GetCurrentDirectoryA( sizeof(path), path.buffer );
		path.append( file );
	}

	void* hmod = nullptr;
	// Need some memory...
	if ( void* base = proc.Alloc( NULL, sizeof(path), MEM_COMMIT, PAGE_READWRITE ) )
	{
		// Write path to remote process
		if ( proc.Write( base, path ) )
		{
			// Call LoadLibraryA in the remote process
			// Address should be the same even with ASLR...
			if ( HANDLE hThread = proc.CreateThread( (LPTHREAD_START_ROUTINE)&LoadLibraryA, base ) )
			{
				// Wait for it to finish
				::WaitForSingleObject( hThread, INFINITE );
				// Get result from exit code
				// FIXME! 32-bit only! This will be tricky as I'll need to actually inject codes to make this work...
				DWORD code;
				::GetExitCodeThread( hThread, &code );
				hmod = (void*)code;
				// Cleanup thread handle
				::CloseHandle( hThread );
			}
		}
		// Cleanup allocated memory
		proc.Free( base, 0, MEM_RELEASE );
	}
	return hmod;
}
bool Manager::WriteTempFile( filesystem::path& path, filesystem::file* pl, const char* hint )
{
	return plsdk::WriteTempFile( path, pl, hint );
}



//------------------------------------------------
// class LocalMM
//------------------------------------------------
LocalMM::LocalMM( bool debug ) : debug(debug), hModule(NULL)
{
#ifndef PLSDK_XSAFESEH
	DWORD flags; BOOL perm;
	hasdep = !( ::GetProcessDEPPolicy( (HANDLE)-1, &flags, &perm ) && (flags&PROCESS_DEP_ENABLE)==0 );
#else
	hasdep = true;
#endif // !PLSDK_XSAFESEH
}
LocalMM::~LocalMM()
{
	Shutdown();
}
NOINLINE bool LocalMM::Init( filesystem::file* pl, const char* hint )
{
	filesystem::path temp;
#ifdef PLSDK_HASDEBUG
	if ( !pl || debug )
	{
		debug = true;

		// Note!
		// Fetching the full path of the payload.dll relative from the WORKING directory!
		// This is different from the default dll search order but allows me to better control which version is loaded for debugging!
		
		::GetCurrentDirectoryA( sizeof(temp), temp.c_str() );
		temp.append( hint );
		if ( hModule = ::LoadLibraryA( temp.buffer ) )
		{
			Exports.Load( hModule );
			return true;
		}
		DWORD err = ::GetLastError();
		return false;
	}
	else
#endif // PLSDK_HASDEBUG
	if ( hasdep )
	{
		if ( WriteTempFile( temp, pl, hint ) )
		{
			if ( hModule = ::LoadLibraryA( temp.buffer ) )
			{
				Exports.Load( hModule );
				return true;
			}
		}
	}
#ifndef PLSDK_XSAFESEH
	else if ( hModule = MapSections( ((HANDLE)-1), pl ) )
	{
		exp.Load( hModule );
		if ( exp.DllMain( hModule, DLL_PROCESS_ATTACH, NULL ) )
			return true;
	}
#endif // !PLSDK_XSAFESEH
	return false;
}
NOINLINE bool LocalMM::Shutdown()
{
	BOOL b = FALSE;
	if ( hModule )
	{
#ifdef PLSDK_HASDEBUG
	if ( debug )
	{
		b = ::FreeLibrary( (HMODULE) hModule );
	}
	else
#endif // PLSDK_HASDEBUG
	{
		HMODULE hmod = (HMODULE) hModule;
		hModule = NULL;

		if ( hasdep )
		{
			// Cleanup the temp file left behind
			filesystem::path path;
			::GetModuleFileNameA( hmod, path.buffer, sizeof(path) );
			b = ::FreeLibrary( hmod );
			b = ::DeleteFileA( path.buffer );
		}
#ifndef PLSDK_XSAFESEH
		else
		{
			b = exp.DllMain( hmod, DLL_PROCESS_DETACH, NULL );
			::VirtualFreeEx( ((HANDLE)-1), hmod, 0, MEM_RELEASE );
		}
#endif // !PLSDK_XSAFESEH
	}
	}
	return b!=FALSE;
}

}
