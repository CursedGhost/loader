#pragma once

//----------------------------------------------------------------
// Payload management
//----------------------------------------------------------------
// Manages payloads and allows interaction.
//
// plsdk::Exports
//  Stores function pointers to the payload's exported functions.
//  These can be called directly when mapped in our local process.
//
// plsdk::Manager
//  Maps the payload in another process, interaction is done through a queue which builds a stub that'll execute the desired functions.
//
// plsdk::LocalMM
//  Maps the payload in the local process.
//

#include <meta/declpfn.h>
#include <filesystem/path.h>
#include <filesystem/file.h>

// Define this to enable debugging support
//#define PLSDK_HASDEBUG
// Define if you can always assume SafeSEH (Deprecated, keep this defined!)
#define PLSDK_XSAFESEH
// Calling convention calling exported payload funcs
#define PLAPI __stdcall

typedef void* HANDLE;
typedef struct tagPROCESSENTRY32 PROCESSENTRY32;
typedef struct tagMODULEENTRY32 MODULEENTRY32;

// Ensures correct usage
#define PLSDK_MANAGER
#ifdef PLSDK_SDK
#error Cannot use both manager and SDK together; The manager is for the loader only!
#endif

namespace plsdk
{

	
//------------------------------------------------
// Payload exports
//------------------------------------------------
// WARNING! KEEP THIS IN SYNC WITH PAYLOAD.DEF !!!

class Exports
{
public:
	void Load( void* hmod );
	void Load( filesystem::file* pl );

	meta::declpfn<int (PLAPI*)( void*, unsigned int, void* )>
		DllMain;
	
	meta::declpfn<int (PLAPI*)()>
		ErrorGet;
	meta::declpfn<void (__cdecl*)( int code, const char* fmt, ... )>
		ErrorSet;
	meta::declpfn<const char* (PLAPI*)()>
		ErrorDesc;

	meta::declpfn<void* (PLAPI*)( const void* bin, unsigned long size, unsigned int flags, unsigned int key )>
		MapDll;
	meta::declpfn<void* (PLAPI*)( const filesystem::file* bin, unsigned int flags )>
		MapDllFile;
	meta::declpfn<bool (PLAPI*)( void* module )>
		UnmapDll;
	meta::declpfn<void* (PLAPI*)( const char* dllname )>
		LoadDllA;
	meta::declpfn<void* (PLAPI*)( const wchar_t* dllname )>
		LoadDllW;
	
	meta::declpfn<int (PLAPI*)( int index, ... )>
		SystemCall;
	meta::declpfn<bool (PLAPI*)( void* pfn )>
		SystemHook;

	meta::declpfn<void* (PLAPI*)( unsigned int name )>
		ModuleHandle;
	meta::declpfn<void* (PLAPI*)( const char* name )>
		ModuleHandleA;
	meta::declpfn<void* (PLAPI*)( const wchar_t* name )>
		ModuleHandleW;
	meta::declpfn<void* (PLAPI*)( void* hmod, unsigned int name )>
		SymAddr;
	meta::declpfn<void* (PLAPI*)( void* hmod, const char* name )>
		SymAddrA;
	meta::declpfn<void* (PLAPI*)( void* hmod, int ordinal )>
		SymOrd;

	meta::declpfn<void (PLAPI*)( void* hmod )>
		CloakModule;
	meta::declpfn<long (PLAPI*)( long seed )>
		PipeThread;
	meta::declpfn<long (__stdcall*)( int, long, long )>
		InjectHookEx;
};

//------------------------------------------------
// class Process
//------------------------------------------------
// Basic wrapper for process handle.

class Process
{
public:
	Process();
	~Process();

	//------------------------------------------------
	// Attach to a process

	// Attach with pid and optional tid.
	bool Attach( DWORD pid, DWORD tid = 0 );
	// Attach to our own process.
	bool Attach();
	// Detach from the current attached process, safe to call even when not attached (in which case it does nothing).
	void Detach();
	// Returns true if we're up and attached to a valid process.
	inline bool isAttached() const { return hProcess!=NULL; }
	// Get the raw platform specific process handle.
	inline HANDLE getHandle() const { return hProcess; }
	// Get the process id of the attached process
	inline DWORD getProcessID() const { return pid; }
	// Get the thread id passed during construction
	// Mostly used when attaching to a process by window handle (GetWindowThreadProcessId)
	// FIXME! Why is this class keeping it around?
	inline DWORD getThreadID() const { return tid; }

	//------------------------------------------------
	// Wrappers around process APIs

	bool Read( LPCVOID ptr, LPVOID store, SIZE_T bytes );
	template< typename T >
	inline bool Read( LPCVOID ptr, T& t ) {
		return Read( ptr, &t, sizeof(T) );
	}
	bool Write( LPVOID dest, LPCVOID src, SIZE_T bytes );
	template< typename T >
	inline bool Write( LPVOID dest, const T& t ) {
		return Write( dest, &t, sizeof(T) );
	}
	LPVOID Alloc( LPVOID ptr, SIZE_T bytes, DWORD type, DWORD prot );
	bool Free( LPVOID ptr, SIZE_T bytes, DWORD type );
	bool Protect( LPVOID ptr, SIZE_T bytes, DWORD prot, DWORD& old );
	bool Query( LPCVOID addr, MEMORY_BASIC_INFORMATION& info );

	HANDLE CreateThread( LPTHREAD_START_ROUTINE pStartRoutine, LPVOID pParam, DWORD* tid = NULL );

	// Enumerate all processes
	// Make sure to keep calling this until it returns false! while ( EnumProcess( ... ) ) for example
	//  name: exe file name, can be nullptr to return on every process
	//  snap: make sure to initialize this to INVALID_HANDLE_VALUE on first call
	static bool EnumProcess( const char* name, HANDLE& snap, PROCESSENTRY32& pe );
	// Enumerate all modules in a process
	// Make sure to keep calling this until it returns false! while ( EnumModules( ... ) ) for example
	//  name: dll file path, can be nullptr to return on every module. Only case about dll filename
	//  snap: make sure to initialize this to INVALID_HANDLE_VALUE on first call
	static bool EnumModules( const char* name, DWORD pid, HANDLE& snap, MODULEENTRY32& me );

protected:
	DWORD pid, tid;
	HANDLE hProcess;
};

//------------------------------------------------
// class Manager
//------------------------------------------------
// Manual mapping in other processes.

class Manager
{
public:
	Manager( Process& proc, bool debug );
	~Manager();

	//------------------------------------------------
	// Interaction system
	//------------------------------------------------
	// This works by dumping the payload (which is usually stored encrypted inside the loader)
	// It is then injected by normal means (CreateRemoteThread/LoadLibrary)

	// Launch the interaction system
	//  pl: handle to the payload file to dump
	//  hint: location to dump the payload file, relative paths will get dumped in temp folder
	bool Q_Begin( filesystem::file* pl, const char* hint = nullptr );
	// Close the connection to the payload, note: does not wait for payload to finish (shouldn't matter)
	void Q_End();

	struct Error
	{
		int code;
		char buf[252];
	};
	// Get the last error code & description from the payload
	void Q_GetError( Error& err );
	// Set the payload error code, mostly for testing purposes
	bool Q_SetError( const char* str );
	// Manual map a dll file in the target process
	void* Q_MapDll( filesystem::file* bin, unsigned flags, Error& err );
	// Call exported function with signature: long (__stdcall*)( const void* )
	long Q_Call( void* hmod, unsigned int func, const void* data, unsigned int len, Error& err );
	
protected:
	bool Q_Send( const void* data, unsigned size );
	template< typename T > inline bool Q_Send( const T& t ) { return Q_Send( &t, sizeof(T) ); }
	bool Q_Recv( void* buf, unsigned size );

	bool Q_Payload( filesystem::path& path, filesystem::file* pl, const char* hint = nullptr );
	bool Q_Launch();
	
// KEEP IN SYNC WITH payload/pipe.h !
enum plcmd_t
{
	Q_PLCMD_EXIT = 0,
	Q_PLCMD_ACK,
	Q_PLCMD_GETERROR,
	Q_PLCMD_SETERROR,
	Q_PLCMD_MAPDLL,
	Q_PLCMD_CALL,
};


public:
	//------------------------------------------------
	// DLL Injector
	//------------------------------------------------
	
	// Inject a dll in the process by CreateRemoteThread / LoadLibrary means, ignore temp_mem
	void* InjectDll( filesystem::hpath path, void* temp_mem = nullptr );
	// Dump a filesystem::file (eg encrypted file) to disk, final path will be stored in path
	// Give a hint to the location with hint, defaults to <temp>/<random>.tmp. Absolute path or filename not required.
	// A single random integer is passed to formatting the 
	static bool WriteTempFile( filesystem::path& path, filesystem::file* pl, const char* hint = nullptr );

private:
	Process& proc;
	bool debug;
	bool hasdep;
	HANDLE hPipe;
	//HANDLE hThread;

	void* hModule;
	Exports exp;
};

//------------------------------------------------
// class LocalMM
//------------------------------------------------
// Manual mapping in your own process.

class LocalMM
{
public:
	LocalMM( bool debug = false );
	~LocalMM();

	// Load the payload
	bool Init( filesystem::file* pl, const char* hint = nullptr );
	// Shutdown
	bool Shutdown();

	// Access the exports
	inline void* Module() const { return hModule; }

private:
	bool debug;
	bool hasdep;
	// The module handle for our payload
	void* hModule;
public:
	// Exported funcs
	Exports Exports;
};


}
