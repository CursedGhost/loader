#pragma once

//----------------------------------------------------------------
// Payload SDK
//----------------------------------------------------------------
// Link your manual mapped modules with this.

#include <cstdarg>
#include <hash/hash.h>
#pragma comment(lib,"payload.lib")

// C wrapper imports

#define PLIMPORT extern "C" __declspec(dllimport)
#define PLAPI __stdcall

// Ensures correct usage
#define PLSDK_SDK
#ifdef PLSDK_MANAGER
#error Cannot use both manager and SDK together; The SDK is for the mapped modules only!
#endif

namespace filesystem
{
	class file;
}

namespace plsdk
{

// Get last error code
PLIMPORT int PLAPI ErrorGet();
// Get last error description
PLIMPORT const char* PLAPI ErrorDesc();
// Set last error and description
PLIMPORT int ErrorSetEx( int code, const char* fmt, va_list va = nullptr );
// Set last error and description
inline int ErrorSet( int code, const char* fmt, ... )
{
	va_list va;
	va_start( va, fmt );
	return ErrorSetEx( code, fmt, va );
}

// MapDll flags
enum mmflag_t
{
	// Load as a resource dll
	MMFL_RESOURCEDLL = (1<<0),
	// By default the mapper will strip some info from the mapped module, this flag keeps everything intact
	MMFL_KEEP_INTACT = (1<<1),
	// Strips most of the PE headers except the absolutely necessary parts to run the dll, might interfere with some dlls
	MMFL_STRIP_PEHEADERS = (1<<2),
	// Do not change the page protections
	MMFL_SKIP_PROTECT = (1<<3),
};
// ManualMaps a pefile in memory
PLIMPORT void* PLAPI MapDll( const void* bin, unsigned long size, unsigned int flags = 0, unsigned int key = 0 );
// ManualMaps an encrypted pefile with maximum security
PLIMPORT void* PLAPI MapDllFile( filesystem::file* file, unsigned int flags = 0 );
// Unloads a dll (manualmapped or not)
PLIMPORT bool PLAPI UnmapDll( void* module );
// Loads a dll from disk (ansi)
PLIMPORT void* PLAPI LoadDllA( const char* dllname );
// Loads a dll from disk (unicode)
PLIMPORT void* PLAPI LoadDllW( const wchar_t* dllname );

// Direct syscalls
PLIMPORT int SystemCall( int idx, ... );
// Hooking syscalls
typedef void (PLAPI* SystemHookFn)( int fn, uintptr_t* args, bool post );
PLIMPORT bool PLAPI SystemHook( SystemHookFn pfn );

// Gets the module handle for a name, looks in the manualmapped module cache, no winapi calls
PLIMPORT void* PLAPI ModuleHandle( unsigned int name );
PLIMPORT void* PLAPI ModuleHandleA( const char* name );
PLIMPORT void* PLAPI ModuleHandleW( const wchar_t* name );
// Grab an exported function address, no winapi calls
PLIMPORT void* PLAPI SymAddr( void* hmod, unsigned int name );
PLIMPORT void* PLAPI SymAddrA( void* hmod, const char* name );
PLIMPORT void* PLAPI SymOrd( void* hmod, int ord );

template< typename T, unsigned L >
inline T SymAddr( void* module, const char (&name)[L] )
{
	return (T) SymAddr( module, HASH(name) );
}
template< typename T >
inline T SymAddrA( void* module, const char* name )
{
	return (T) SymAddrA( module, name );
}

PLIMPORT void PLAPI DestroyInitCode( void* func );

}
